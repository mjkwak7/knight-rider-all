//
//  RegisterVC.swift
//  Knight Rider
//
//  Created by Michael K. on 6/30/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit
import Alamofire
import GooglePlaces

class RegisterVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstName.delegate = self
        lastName.delegate = self
        email.delegate = self
        address.addTarget(self, action: #selector(addressEditing(sender:)), for: .editingDidBegin)
        phone.delegate = self
        password.delegate = self
        confirmPassword.delegate = self
        self.hideKeyboardWhenTappedAround()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
        
    }
    
    func addressEditing(sender:UITextField) -> Void {
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
        
    }
    
    @IBAction func registerPressed(_ sender: Any) {
        
        if firstName.text != "" && lastName.text != "" && email.text != "" && password.text != "" && confirmPassword.text != "" {
            
            if password.text == confirmPassword.text {
                
                let registerParameters: Parameters = [
                    "firstName" : firstName.text!.capitalized,
                    "lastName" : lastName.text!.capitalized,
                    "email" : email.text!,
                    "address": address.text!,
                    "phone": phone.text!,
                    "password" : password.text!,
                    "matchingPassword": confirmPassword.text!,
                ]
                
                Alamofire.request(REGISTER_URL, method: .post, parameters: registerParameters, encoding: JSONEncoding.default, headers: regularHeaders).responseJSON { response in
                    
                    if let dict = response.result.value as? Dictionary<String, AnyObject> {
                        
                        if let message = dict["message"] as? String {
                            
                            if message == "Success. Please check your email." {
                                
                                let alert = UIAlertController(title: "Success!", message: "Please check your email to confirm your account. Once you've done that, you can log in and start using the app.", preferredStyle: UIAlertControllerStyle.actionSheet)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                                    alert.dismiss(animated: true, completion: nil)
                                    self.firstName.text = ""
                                    self.lastName.text = ""
                                    self.email.text = ""
                                    self.password.text = ""
                                    self.confirmPassword.text = ""
                                    self.dismiss(animated: true, completion: nil)
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                
                            } else {
                                
                                let alert = UIAlertController(title: "Registration error. Please check your information and try again.", message: "", preferredStyle: UIAlertControllerStyle.alert)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                                    alert.dismiss(animated: true, completion: nil)
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        } else {
                            
                            let alert = UIAlertController(title: "Sever error. Please try again later.", message: "", preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                                alert.dismiss(animated: true, completion: nil)
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        
                    }
                
                }
                
            } else {
                
                let alert = UIAlertController(title: "Passwords do not match. Please try again.", message: "", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            }
            
        } else {
            
            let alert = UIAlertController(title: "Please fill in all fields.", message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                alert.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func alreadyHaveAccountPressed(_ sender: Any) {
        firstName.text = ""
        lastName.text = ""
        email.text = ""
        address.text = ""
        phone.text = ""
        password.text = ""
        confirmPassword.text = ""
        dismiss(animated: true, completion: nil)
    }
        
}

extension RegisterVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        address.text = place.formattedAddress
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

