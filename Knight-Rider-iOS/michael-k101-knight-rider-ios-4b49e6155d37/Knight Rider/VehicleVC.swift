//
//  VehicleVC.swift
//  Knight Rider
//
//  Created by Michael K. on 8/4/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit
import Alamofire
import SwiftKeychainWrapper

class VehicleVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var cars = [Car]()
    let UID = KeychainWrapper.standard.string(forKey: UID_KEY)
    let TOKEN = KeychainWrapper.standard.string(forKey: TOKEN_KEY)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        downloadCarData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleCell", for: indexPath) as? VehicleCell {
            
            let car = cars[indexPath.row]
            cell.deleteButton.tag = indexPath.row
            cell.deleteButton.addTarget(self, action: #selector(deleteCar), for: .touchUpInside)
            cell.configureCell(car: car)
            return cell
            
        } else {
            
            return VehicleCell()
            
        }
        
    }
    
    func downloadCarData() {
        
        cars.removeAll()
        
        let tokenHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Authorization": "Bearer \(TOKEN!)",
            "Cache-Control": "no-cache"
        ]
        
        Alamofire.request(CARS_URL + "\(UID!)/cars", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { response in
            
            if let array = response.result.value as? [Dictionary<String, AnyObject>] {
                
                for obj in array {
                    let car = Car(carsDict: obj)
                    self.cars.append(car)
                }
                
                self.tableView.reloadData()
                
            }
            
        }
        
    }
    
    func deleteCar(_ sender: UIButton) {
        var selIndex: Int!
        var carId: Int!
        
        if let cell = sender.superview?.superview?.superview?.superview as? VehicleCell {
            selIndex = tableView.indexPath(for: cell)?.row
            carId = cars[selIndex].id
            print(selIndex)
            print(carId)
        }
        
        let tokenHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Authorization": "Bearer \(TOKEN!)",
            "Cache-Control": "no-cache"
        ]
        
        Alamofire.request(ALL_CARS_URL + "\(carId!)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { response in
            
            print(response.debugDescription)
            
            if let dict = response.result.value as? Dictionary<String, AnyObject> {
                
                if let message = dict["message"] as? String {
                    
                    if message == "Successfully deleted!" {
                        
                        let alert = UIAlertController(title: "Car deleted.", message: "", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                            alert.dismiss(animated: true, completion: nil)
                            self.downloadCarData()
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                } else {
                    
                    let alert = UIAlertController(title: "Server error. Please try again later.", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                
                }
                    
            }
            
        }
        
    }

}
