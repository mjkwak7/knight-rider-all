//
//  RoundedCorners.swift
//  Knight Rider
//
//  Created by Michael K. on 7/29/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit

class RoundedCorners: UIView {

    override func layoutSubviews() {
        layer.cornerRadius = 7
    }

}
