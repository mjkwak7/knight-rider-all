//
//  ProfileVC.swift
//  Knight Rider
//
//  Created by Michael K. on 8/4/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

    @IBOutlet weak var personalInfoView: UIView!
    @IBOutlet weak var vehicleInfoView: UIView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var addCar: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addCar.isEnabled = false
        addCar.tintColor = UIColor(red: 99, green: 51, blue: 147, alpha: 0)
    }
    
    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        
        switch segment.selectedSegmentIndex {
        case 0:
            vehicleInfoView.isHidden = true
            personalInfoView.isHidden = false
            addCar.isEnabled = false
            addCar.tintColor = UIColor(red: 99, green: 51, blue: 147, alpha: 0)
        case 1:
            vehicleInfoView.isHidden = false
            personalInfoView.isHidden = true
            addCar.isEnabled = true
            addCar.tintColor = UIColor.white
        default:
            break;
        }
        
    }

}
