//
//  LoginVC.swift
//  Knight Rider
//
//  Created by Michael K. on 6/25/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit
import Alamofire
import SwiftKeychainWrapper

class LoginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        email.delegate = self
        password.delegate = self
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let _ = KeychainWrapper.standard.string(forKey: UID_KEY) {
            performSegue(withIdentifier: "goToRides", sender: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }

    @IBAction func loginPressed(_ sender: Any) {
        
        let email = self.email.text!
        let password = self.password.text!
        
        if email != "" && password != "" {
            
            let loginParameters: Parameters = [
                "username": "\(email)",
                "password": "\(password)"
            ]
            
            Alamofire.request(LOGIN_URL, method: .post, parameters: loginParameters, encoding: JSONEncoding.default, headers: regularHeaders).responseJSON { response in
                
                print(response.debugDescription)
                
                if let dict = response.result.value as? Dictionary<String, AnyObject> {
                    
                    if let userId = dict["user_id"] as? String {
                        
                        KeychainWrapper.standard.set(userId, forKey: UID_KEY)
                        
                        if let token = dict["token"] as? String {
                            KeychainWrapper.standard.set(token, forKey: TOKEN_KEY)
                        }

                        if let refreshToken = dict["refresh_token"] as? String {
                            KeychainWrapper.standard.set(refreshToken, forKey: REFRESH_TOKEN_KEY)
                        }
                        
                        self.email.text = ""
                        self.password.text = ""
                        self.performSegue(withIdentifier: "goToRides", sender: nil)
                        
                    } else {
                        
                        let alert = UIAlertController(title: "Incorrect credentials or email not validated. Please try again.", message: "", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                            alert.dismiss(animated: true, completion: nil)
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                } else {
                    
                    let alert = UIAlertController(title: "Server error. Please try again later.", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            }
            
        } else {
            
            let alert = UIAlertController(title: "Please enter username and password.", message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                alert.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func registerPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToRegisterVC", sender: nil)
    }
    
}




