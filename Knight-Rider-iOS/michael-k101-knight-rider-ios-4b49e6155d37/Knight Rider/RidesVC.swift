//
//  RidesVC.swift
//  Knight Rider
//
//  Created by Michael K. on 6/26/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit
import Alamofire
import SwiftKeychainWrapper

class RidesVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var rides = [Ride]()
    var selectedIndex = Int()
    var locationManager: CLLocationManager?
    var currentLatitude: Double!
    var currentLongitude: Double!
    var currentSpeed: Double!
    let UID = KeychainWrapper.standard.string(forKey: UID_KEY)
    let TOKEN = KeychainWrapper.standard.string(forKey: TOKEN_KEY)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        self.locationManager?.requestAlwaysAuthorization()
        self.locationManager?.desiredAccuracy = kCLLocationAccuracyHundredMeters
        self.locationManager?.startUpdatingLocation()
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.uploadLocation), userInfo: nil, repeats: true)
        getUserInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        downloadRidesData()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print("didFailWithError: \(error.localizedDescription)")
        
        let alert = UIAlertController(title: "Error.", message: "Failed to get your location.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLatitude = locations.last?.coordinate.latitude
        currentLongitude = locations.last?.coordinate.longitude
        currentSpeed = (locations.last?.speed)! * 2.23694
    }
    
    func getUserInfo() {
        
        let tokenHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Authorization": "Bearer \(TOKEN!)",
            "Cache-Control": "no-cache"
        ]
        
        Alamofire.request(USER_URL + "\(UID!)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { (response) in
            
            print(response.debugDescription)
            
            if let user = response.result.value as? Dictionary<String, AnyObject> {
                if let address = user["address"] as? String {
                    KeychainWrapper.standard.set(address, forKey: ADDRESS_KEY)
                }
            }
            
        }
        
    }
    
    func uploadLocation() {
        
        let parameters: Parameters = [
            "latitude": currentLatitude,
            "longitude": currentLongitude,
            "speed": currentSpeed
        ]
        
        let tokenHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Authorization": "Bearer \(TOKEN!)",
            "Cache-Control": "no-cache"
        ]
        
        Alamofire.request(LOCATIONS_URL + "\(UID!)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { (response) in
                            
            if let result = response.result.value as? Dictionary<String, AnyObject> {
                if let message = result["message"] as? String {
                    if message == "Successfully added!" {

                    }
                }
            }
            
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rides.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "RideCell", for: indexPath) as? RideCell {
            
            let ride = rides[indexPath.row]
            cell.selectionStyle = .none
            cell.configureCell(ride: ride)
            return cell
            
        } else {
            
            return RideCell()
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.performSegue(withIdentifier: "goToRideDetail", sender: self)
    }
    
    func downloadRidesData() {
        
        rides.removeAll()
        
        let tokenHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Authorization": "Bearer \(TOKEN!)",
            "Cache-Control": "no-cache"
        ]
        
        Alamofire.request(RIDES_URL + "\(UID!)/trips", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { response in
            
            if let array = response.result.value as? [Dictionary<String, AnyObject>] {
                
                for obj in array {
                    let ride = Ride(ridesDict: obj)
                    self.rides.append(ride)
                }
                
                self.tableView.reloadData()
                
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToRideDetail" {
            
            let nextVC = segue.destination as? RideDetailVC
            
            for ride in self.rides {
                
                if ride.id == rides[selectedIndex].id {
                    nextVC?.ride = ride
                }
                
            }
            
        }
        
    }

    @IBAction func logoutPressed(_ sender: Any) {
        KeychainWrapper.standard.removeObject(forKey: UID_KEY)
        KeychainWrapper.standard.removeObject(forKey: TOKEN_KEY)
        KeychainWrapper.standard.removeObject(forKey: REFRESH_TOKEN_KEY)
        dismiss(animated: true, completion: nil)
    }

}

