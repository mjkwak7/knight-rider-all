//
//  PersonalVC.swift
//  Knight Rider
//
//  Created by Michael K. on 8/8/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit
import GooglePlaces
import Alamofire
import AlamofireImage
import SwiftKeychainWrapper

//Flaw in zip code.

class PersonalVC: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var profilePhoto: ProfileCircleView!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var phone: UITextField!
    
    var picker: UIImagePickerController!
    var zip: String?
    var profilePictureURL: URL!
    var profilePic: UIImage!
    let UID = KeychainWrapper.standard.string(forKey: UID_KEY)
    let TOKEN = KeychainWrapper.standard.string(forKey: TOKEN_KEY)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstName.delegate = self
        lastName.delegate = self
        address.delegate = self
        phone.delegate = self
        
        picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        
        address.addTarget(self, action: #selector(addressEditing(sender:)), for: .editingDidBegin)
        downloadUserInfo()
        self.hideKeyboardWhenTappedAround()
    }
    
    func addressEditing(sender:UITextField) -> Void {
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            let parameters: Parameters = [
                "extension": "jpg",
                "profilePicture": encodePhoto(photo: image)
            ]
            
            let tokenHeaders: HTTPHeaders = [
                "Content-Type": "application/json",
                "X-Authorization": "Bearer \(TOKEN!)",
                "Cache-Control": "no-cache"
            ]
            
            Alamofire.request(PROFILE_PIC_URL + "\(UID!)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { response in
                
                if let dict = response.result.value as? Dictionary<String, AnyObject> {
                    
                    if let profilePicture = dict["profilePicture"] as? String {
                        
                        self.profilePictureURL = URL(string: profilePicture)
                        self.profilePhoto.image = image
                        
                    }
                    
                }
                
            }
   
        }
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func downloadUserInfo() {
        
        let tokenHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Authorization": "Bearer \(TOKEN!)",
            "Cache-Control": "no-cache"
        ]
        
        Alamofire.request(USER_URL + "\(UID!)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { response in
            
            if let dict = response.result.value as? Dictionary<String, AnyObject> {
                
                let user = User(userDict: dict)
                
                self.firstName.text = user.firstName
                self.lastName.text = user.lastName
                self.username.text = user.username
                self.address.text = user.address
                self.zip = user.zip
                self.phone.text = user.phone
                
                if user.profilePicture != "" {
                    
                    self.profilePictureURL = URL(string: user.profilePicture)
                    
                    Alamofire.request(self.profilePictureURL).responseImage { response in
                        if let image = response.result.value {
                            print(response.debugDescription)
                            self.profilePhoto.image = image
                        }
                    }
                   
                } else {
                    
                    let alert = UIAlertController(title: "Please update your profile photo.", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            }
            
        }
        
    }
    
    func encodePhoto(photo: UIImage) -> String {
        let imageData = UIImageJPEGRepresentation(photo, 1)
        let strBase64 = imageData?.base64EncodedString()
        return strBase64!
    }
    
    @IBAction func editPhotoPressed(_ sender: Any) {
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func updatePressed(_ sender: Any) {
        
        if firstName.text != "" && lastName.text != "" && username.text != "" && address.text != "" && phone.text != "" {
            
            let parameters: Parameters = [
                "firstName" : firstName.text!.capitalized,
                "lastName" : lastName.text!.capitalized,
                "username": username.text!,
                "address": address.text!,
                "zip": zip ?? "",
                "phone": phone.text!,
            ]
            
            let tokenHeaders: HTTPHeaders = [
                "Content-Type": "application/json",
                "X-Authorization": "Bearer \(TOKEN!)",
                "Cache-Control": "no-cache"
            ]
            
            Alamofire.request(USER_URL + "\(UID!)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { response in
                
                if let dict = response.result.value as? Dictionary<String, AnyObject> {
                    
                    if self.firstName.text == dict["firstName"] as? String {
                        
                        if self.lastName.text == dict["lastName"] as? String {
                         
                            if self.username.text == dict["username"] as? String {
                             
                                if self.address.text == dict["address"] as? String {
                                 
                                    if self.zip == dict["zip"] as? String {
                                     
                                        if self.phone.text == dict["phone"] as? String {
                                            
                                            let alert = UIAlertController(title: "Info updated successfully.", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
                                            
                                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                                                alert.dismiss(animated: true, completion: nil)
                                            }))
                                            
                                            self.present(alert, animated: true, completion: nil)
                                            
                                        }
                                            
                                    }
                                        
                                }
                                    
                            }
                                
                        }
                            
                    }
                        
                } else {
                    
                    let alert = UIAlertController(title: "Update failed. Please try again later.", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                    
            }
            
        } else {
            
            let alert = UIAlertController(title: "Please update all fields before you continue.", message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                alert.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
}

extension PersonalVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        address.text = place.formattedAddress
        
        for component in place.addressComponents! {
            if component.type == "postal_code" {
                self.zip = component.name
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
