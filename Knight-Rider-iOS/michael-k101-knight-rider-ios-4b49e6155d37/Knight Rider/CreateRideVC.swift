//
//  CreateRideVC.swift
//  Knight Rider
//
//  Created by Michael K. on 7/24/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit
import Alamofire
import GooglePlaces
import CoreLocation
import SwiftKeychainWrapper

class CreateRideVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var originAddressTextField: UITextField!
    @IBOutlet weak var destinationAddressTextField: UITextField!
    @IBOutlet weak var dateAndDepartureTime: UITextField!
    @IBOutlet weak var car: UITextField!
    
    var cars = [Car]()
    var campuses = ["Macon Campus", "Cochran Campus", "Eastman Campus", "Warner Robins Campus", "Dublin Campus"]
    var originPressed = false
    var destinationPressed = false
    var carPressed = false
    var originCity: String!
    var destCity: String!
    var timeStamp: Double!
    var carId: Int!
    var availableSeats: Int!
    var originAddress: String!
    var destinationAddress: String!
    let UID = KeychainWrapper.standard.string(forKey: UID_KEY)
    let TOKEN = KeychainWrapper.standard.string(forKey: TOKEN_KEY)
    
    let datePickerView = UIDatePicker()
    let carPickerView = UIPickerView()
    let campusPickerView = UIPickerView()
    let coder = CLGeocoder()
    var originCoordinate = CLLocationCoordinate2D()
    var destCoordinate = CLLocationCoordinate2D()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        carPickerView.delegate = self
        campusPickerView.delegate = self
        car.inputView = carPickerView
        originAddressTextField.inputView = campusPickerView
        destinationAddressTextField.inputView = campusPickerView
        
        originAddressTextField.addTarget(self, action: #selector(originEditing(sender:)), for: .editingDidBegin)
        originAddressTextField.addTarget(self, action: #selector(convertOriginAddress), for: .editingDidEnd)
        destinationAddressTextField.addTarget(self, action: #selector(destinationEditing(sender:)), for: .editingDidBegin)
        destinationAddressTextField.addTarget(self, action: #selector(convertDestAddress), for: .editingDidEnd)
        car.addTarget(self, action: #selector(carEditing(sender:)), for: .editingDidBegin)
        
        downloadCarsData()
        self.hideKeyboardWhenTappedAround()
    }
    
    func downloadCarsData() {
        
        let tokenHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Authorization": "Bearer \(TOKEN!)",
            "Cache-Control": "no-cache"
        ]
        
        Alamofire.request(CARS_URL + "\(UID!)/cars", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { response in
            
            if let array = response.result.value as? [Dictionary<String, AnyObject>] {
                
                for obj in array {
                    let car = Car(carsDict: obj)
                    self.cars.append(car)
                }
                
            }
            
        }
        
    }
    
    func originEditing(sender:UITextField) -> Void {
        
        originPressed = true
        destinationPressed = false
        sender.inputView = campusPickerView
        
    }
    
    func destinationEditing(sender:UITextField) -> Void {
        
        destinationPressed = true
        originPressed = false
        sender.inputView = campusPickerView
        
    }

    func carEditing(sender:UITextField) -> Void {
        
        destinationPressed = false
        originPressed = false
        carPressed = true
        sender.inputView = carPickerView
        
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, YYYY " + "-" + " h:mm a"
        dateAndDepartureTime.text = dateFormatter.string(from: sender.date)
        timeStamp = self.datePickerView.date.timeIntervalSince1970
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if originPressed {
            return campuses.count
        } else if destinationPressed {
            return campuses.count
        } else {
            return cars.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if originPressed {
            return campuses[row]
        } else if destinationPressed {
            return campuses[row]
        } else {
            return cars[row].maker + " " + cars[row].type
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if originPressed {
            originAddressTextField.text = campuses[pickerView.selectedRow(inComponent: 0)]
        } else if destinationPressed {
            destinationAddressTextField.text = campuses[pickerView.selectedRow(inComponent: 0)]
        } else {
            car.text = cars[pickerView.selectedRow(inComponent: 0)].maker + " " + cars[pickerView.selectedRow(inComponent: 0)].type
            carId = cars[pickerView.selectedRow(inComponent: 0)].id
            availableSeats = cars[pickerView.selectedRow(inComponent: 0)].capacity
        }
        
        
    }
    
    @IBAction func dateEditing(_ sender: UITextField) {
        
        datePickerView.datePickerMode = .dateAndTime
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createRidePressed(_ sender: Any) {
        
        if originAddressTextField.text != "" && destinationAddressTextField.text != "" && dateAndDepartureTime.text != "" && car.text != "" {
            
            print(self.originCoordinate.latitude)
            print(self.originCoordinate.longitude)
            
            let originAddress = self.originAddress
            let destinationAddress = self.destinationAddress
            let originCity = self.originCity
            let destCity = self.destCity
            let originCoordinate = self.originCoordinate
            let destCoordinate = self.destCoordinate
            let timestamp = self.timeStamp * 1000
            let carId = self.carId
            let meetingLocation = self.originCity
            let availableSeats = self.availableSeats

            let rideParameters: Parameters = [
                "carId": carId!,
                "driverId": UID!,
                "originAddress": originAddress!,
                "originCity": originCity!,
                "originLatitude": originCoordinate.latitude,
                "originLongitude": originCoordinate.longitude,
                "destAddress": destinationAddress!,
                "destCity": destCity!,
                "destLatitude": destCoordinate.latitude,
                "destLongitude": destCoordinate.longitude,
                "departureTime": timestamp,
                "meetingLocation": meetingLocation!,
                "availableSeats": availableSeats!
            ]
            
            let tokenHeaders: HTTPHeaders = [
                "Content-Type": "application/json",
                "X-Authorization": "Bearer \(TOKEN!)",
                "Cache-Control": "no-cache"
            ]
            
            Alamofire.request(RIDES_URL + "\(UID!)/trips", method: .post, parameters: rideParameters, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { response in
                
                if let dict = response.result.value as? Dictionary<String, AnyObject> {
                    
                    if let message = dict["message"] as? String {
                        
                        if message == "Successfully created!" {
                            
                            self.originAddressTextField.text = ""
                            self.destinationAddressTextField.text = ""
                            self.dateAndDepartureTime.text = ""
                            self.car.text = ""
                            self.dismiss(animated: true, completion: nil)
                            
                        }
                        
                    } else {
                        
                        let alert = UIAlertController(title: "Server error. Please try again later.", message: "", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }
                
            }
            
        } else {
            
            let alert = UIAlertController(title: "Please fill in all information regarding your ride.", message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                alert.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    func convertOriginAddress() {
        
        var oAddress:String!
        
        switch self.originAddressTextField.text! {
        case "Macon Campus":
            self.originAddress = "100 University Pkwy, Macon, GA 31206"
            self.originCity = "Macon"
            oAddress = "100 University Pkwy, Macon, GA 31206"
        case "Cochran Campus":
            self.originAddress = "1100 SE 2nd St, Cochran, GA 31014"
            self.originCity = "Cochran"
            oAddress = "1100 SE 2nd St, Cochran, GA 31014"
        case "Eastman Campus":
            self.originAddress = "71 Airport Rd, Eastman, GA 31023"
            self.originCity = "Eastman"
            oAddress = "71 Airport Rd, Eastman, GA 31023"
        case "Warner Robins Campus":
            self.originAddress = "100 University Boulevard, Warner Robins, GA 31093"
            self.originCity = "Warner Robins"
            oAddress = "100 University Boulevard, Warner Robins, GA 31093"
        case "Dublin Campus":
            self.originAddress = "1900 Bellevue Ave, Dublin, GA 31021"
            self.originCity = "Dublin"
            oAddress = "1900 Bellevue Ave, Dublin, GA 31021"
        default:
            break
        }
        
        coder.geocodeAddressString(oAddress) { (response, error) in
            let latitude = (response?[0].location?.coordinate.latitude)!
            let longitude = (response?[0].location?.coordinate.longitude)!
            self.originCoordinate.latitude = latitude
            self.originCoordinate.longitude = longitude
        }
        
    }
    
    func convertDestAddress() {
        
        var dAddress:String!
        
        switch self.destinationAddressTextField.text! {
        case "Macon Campus":
            self.destinationAddress = "100 University Pkwy, Macon, GA 31206"
            self.destCity = "Macon"
            dAddress = "100 University Pkwy, Macon, GA 31206"
        case "Cochran Campus":
            self.destinationAddress = "1100 SE 2nd St, Cochran, GA 31014"
            self.destCity = "Cochran"
            dAddress = "1100 SE 2nd St, Cochran, GA 31014"
        case "Eastman Campus":
            self.destinationAddress = "71 Airport Rd, Eastman, GA 31023"
            self.destCity = "Eastman"
            dAddress = "71 Airport Rd, Eastman, GA 31023"
        case "Warner Robins Campus":
            self.destinationAddress = "100 University Boulevard, Warner Robins, GA 31093"
            self.destCity = "Warner Robins"
            dAddress = "100 University Boulevard, Warner Robins, GA 31093"
        case "Dublin Campus":
            self.destinationAddress = "1900 Bellevue Ave, Dublin, GA 31021"
            self.destCity = "Dublin"
            dAddress = "1900 Bellevue Ave, Dublin, GA 31021"
        default:
            break
        }
        
        coder.geocodeAddressString(dAddress) { (response, error) in
            let latitude = (response?[0].location?.coordinate.latitude)!
            let longitude = (response?[0].location?.coordinate.longitude)!
            self.destCoordinate.latitude = latitude
            self.destCoordinate.longitude = longitude
        }
        
    }
    
}
