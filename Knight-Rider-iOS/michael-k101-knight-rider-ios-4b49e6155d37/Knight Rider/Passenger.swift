//
//  Passengers.swift
//  Knight Rider
//
//  Created by Michael K. on 8/2/17.
//  Copyright © 2017 MGA. All rights reserved.
//

//
//  Driver.swift
//  Knight Rider
//
//  Created by Michael K. on 8/2/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import Foundation

class Passenger {
    
    private var _id: Int!
    private var _username: String!
    private var _firstName: String!
    private var _lastName: String!
    private var _address: String!
    private var _zip: String!
    private var _phone: String!
    private var _profilePicture: String!
    
    var id: Int {
        if _id == nil {
            _id = 0
        }
        return _id
    }
    
    var username: String {
        if _username == nil {
            _username = ""
        }
        return _username
    }
    
    var firstName: String {
        if _firstName == nil {
            _firstName = ""
        }
        return _firstName
    }
    
    var lastName: String {
        if _lastName == nil {
            _lastName = ""
        }
        return _lastName
    }
    
    var address: String {
        if _address == nil {
            _address = ""
        }
        return _address
    }
    
    var zip: String {
        if _zip == nil {
            _zip = ""
        }
        return _zip
    }
    
    var phone: String {
        if _phone == nil {
            _phone = ""
        }
        return _phone
    }
    
    var profilePicture: String {
        if _profilePicture == nil {
            _profilePicture = ""
        }
        return _profilePicture
    }
    
    init(passengerDict: Dictionary<String, AnyObject>) {
        
        if let id = passengerDict["id"] as? Int {
            self._id = id
        }
        
        if let username = passengerDict["username"] as? String {
            self._username = username
        }
        
        if let firstName = passengerDict["firstName"] as? String {
            self._firstName = firstName
        }
        
        if let lastName = passengerDict["lastName"] as? String {
            self._lastName = lastName
        }
        
        if let address = passengerDict["address"] as? String {
            self._address = address
        }
        
        if let zip = passengerDict["zip"] as? String {
            self._zip = zip
        }
        
        if let phone = passengerDict["phone"] as? String {
            self._phone = phone
        }
        
        if let profilePicture = passengerDict["profilePicture"] as? String {
            self._profilePicture = profilePicture
        }
        
    }
    
}
