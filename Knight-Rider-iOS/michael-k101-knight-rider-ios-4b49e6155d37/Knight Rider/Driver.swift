//
//  Driver.swift
//  Knight Rider
//
//  Created by Michael K. on 8/2/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import Foundation

class Driver {
    
    private var _driverId: Int!
    private var _username: String!
    private var _firstName: String!
    private var _lastName: String!
    private var _address: String!
    private var _zip: String!
    private var _phone: String!
    private var _profilePicture: String!

    var driverId: Int {
        if _driverId == nil {
            _driverId = 0
        }
        return _driverId
    }
    
    var username: String {
        if _username == nil {
            _username = ""
        }
        return _username
    }
    
    var firstName: String {
        if _firstName == nil {
            _firstName = ""
        }
        return _firstName
    }
    
    var lastName: String {
        if _lastName == nil {
            _lastName = ""
        }
        return _lastName
    }
    
    var address: String {
        if _address == nil {
            _address = ""
        }
        return _address
    }
    
    var zip: String {
        if _zip == nil {
            _zip = ""
        }
        return _zip
    }
    
    var phone: String {
        if _phone == nil {
            _phone = ""
        }
        return _phone
    }
    
    var profilePicture: String {
        if _profilePicture == nil {
            _profilePicture = ""
        }
        return _profilePicture
    }
    
    init(driverDict: Dictionary<String, AnyObject>) {
        
        if let driverId = driverDict["driverId"] as? Int {
            self._driverId = driverId
        }
        
        if let username = driverDict["username"] as? String {
            self._username = username
        }
        
        if let firstName = driverDict["firstName"] as? String {
            self._firstName = firstName
        }
        
        if let lastName = driverDict["lastName"] as? String {
            self._lastName = lastName
        }
        
        if let address = driverDict["address"] as? String {
            self._address = address
        }
        
        if let zip = driverDict["zip"] as? String {
            self._zip = zip
        }
        
        if let phone = driverDict["phone"] as? String {
            self._phone = phone
        }
        
        if let profilePicture = driverDict["profilePicture"] as? String {
            self._profilePicture = profilePicture
        }
        
    }

}
