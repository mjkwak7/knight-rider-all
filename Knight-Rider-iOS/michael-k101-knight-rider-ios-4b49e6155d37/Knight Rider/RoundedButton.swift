//
//  RoundedButton.swift
//  Knight Rider
//
//  Created by Michael K. on 8/2/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 7
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.white.cgColor
    }
    
}
