//
//  FindRideVC.swift
//  Knight Rider
//
//  Created by Michael K. on 7/24/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftKeychainWrapper

class FindRideVC: UIViewController, UITextFieldDelegate, GMSMapViewDelegate {
    
    @IBOutlet weak var mapWindow: GMSMapView!
    @IBOutlet weak var fromLocation: UITextField!
    @IBOutlet weak var toLocation: UITextField!
    @IBOutlet weak var verticalConstraint: NSLayoutConstraint!
    @IBOutlet var customInfoWindow: UIView!
    
    @IBOutlet weak var originCity: UILabel!
    @IBOutlet weak var destCity: UILabel!
    @IBOutlet weak var originAddress: UILabel!
    @IBOutlet weak var departureDateAndTime: UILabel!
    @IBOutlet weak var destinationAddress: UILabel!
    @IBOutlet weak var profilePicture: CircleView!
    @IBOutlet weak var driver: UILabel!
    @IBOutlet weak var remainingSeats: UILabel!
    @IBOutlet weak var messages: UILabel!
    
    var searchedRides = [Ride]()
    var fromLocationText = ""
    var toLocationText = ""
    var markerId: String!
    let TOKEN = KeychainWrapper.standard.string(forKey: TOKEN_KEY)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        toLocation.delegate = self
        fromLocation.delegate = self
        mapWindow.delegate = self
        mapWindow.camera = GMSCameraPosition.camera(withLatitude: 32.387446, longitude: -83.352288, zoom: 9)
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateSearchedRidesInfo()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func updateSearchedRidesInfo() {
        
        searchedRides.removeAll()
        
        var searchParameters: Parameters!
        
        if fromLocationText != "" || toLocationText != "" {
            
            searchParameters = [
                "origin": fromLocationText,
                "dest": toLocationText
            ]
            
        } else {
            
            searchParameters = nil
            
        }
        
        let tokenHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Authorization": "Bearer \(TOKEN!)",
            "Cache-Control": "no-cache"
        ]
        
        Alamofire.request(TRIPS_SEARCH_URL, method: .post, parameters: searchParameters, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { response in
                        
            if let array = response.result.value as? [Dictionary<String, AnyObject>] {
                
                if !array.isEmpty {
                    
                    for obj in array {
                        let ride = Ride(ridesDict: obj)
                        self.searchedRides.append(ride)
                    }
                    
                }
                
            }
            
        }

    }
    
    func configureMap() {
        
        searchedRides.removeAll()
        mapWindow.clear()
        
        let searchParameters: Parameters = [
            "origin": fromLocationText,
            "dest": toLocationText
        ]
        
        let tokenHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Authorization": "Bearer \(TOKEN!)",
            "Cache-Control": "no-cache"
        ]
        
        Alamofire.request(TRIPS_SEARCH_URL, method: .post, parameters: searchParameters, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { response in
            
            if let array = response.result.value as? [Dictionary<String, AnyObject>] {
                
                if !array.isEmpty {
                    
                    for obj in array {
                        let ride = Ride(ridesDict: obj)
                        self.searchedRides.append(ride)
                    }
                    
                    for ride in self.searchedRides {
                        let marker1 = GMSMarker()
                        marker1.position = CLLocationCoordinate2DMake(ride.originLatitude, ride.originLongitude)
                        marker1.snippet = String(ride.id)
                        marker1.map = self.mapWindow
                        
                        let marker2 = GMSMarker()
                        marker2.position = CLLocationCoordinate2DMake(ride.destLatitude, ride.destLongitude)
                        marker2.snippet = String(ride.id)
                        marker2.map = self.mapWindow
                        
                        let path = GMSMutablePath()
                        path.addLatitude(ride.originLatitude, longitude: ride.originLongitude)
                        path.addLatitude(ride.destLatitude, longitude: ride.destLongitude)
                        
                        let polyline = GMSPolyline(path: path)
                        polyline.strokeColor = .purple
                        polyline.strokeWidth = 5.0
                        polyline.map = self.mapWindow
                    }
                    
                    self.verticalConstraint.constant = 550
                    UIView.animate(withDuration: 0.5) {
                        self.view.layoutIfNeeded()
                    }
                    
                    self.fromLocation.text = ""
                    self.toLocation.text = ""
                    self.view.endEditing(true)
                    
                } else {
                    
                    let alert = UIAlertController(title: "No rides with specified criteria found.", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            } else {
                
                let alert = UIAlertController(title: "Server error. Please try again later.", message: "", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return UIView()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        customInfoWindow.removeFromSuperview()
        customInfoWindow.alpha = 0
        markerId = marker.snippet!
        
        for ride in searchedRides {
            if markerId == String(ride.id) {
                originCity.text = ride.originCity
                destCity.text = ride.destCity
                originAddress.text = ride.originAddress
                departureDateAndTime.text = ride.departureTime
                destinationAddress.text = ride.destAddress
                driver.text = ride.driver.firstName + " " + ride.driver.lastName
                remainingSeats.text = String(ride.remainingSeats)
                messages.text = String(ride.messages.count)
                
                if !ride.driver.profilePicture.isEmpty {
                    let urlRequest = URL(string: ride.driver.profilePicture)
                    Alamofire.request(urlRequest!).responseImage(completionHandler: { (response) in
                        print(response.debugDescription)
                        if let image = response.result.value {
                            self.profilePicture.image = image
                        } else {
                            self.profilePicture.image = UIImage()
                        }
                    })
                }
            }
        }
        
        self.mapWindow.addSubview(self.customInfoWindow)
        
        UIView.animate(withDuration: 0.25) {
            self.customInfoWindow.alpha = 1
        }
        
        return false
        
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        if (mapWindow.selectedMarker != nil) {
            let location = mapWindow.selectedMarker?.position
            customInfoWindow.center = mapWindow.projection.point(for: location!)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetails" {
            
            let nextVC = segue.destination as? RideDetailVC
            
            for ride in searchedRides {
                if markerId == String(ride.id) {
                    nextVC?.ride = ride
                }
            }
        
        }
        
    }

    @IBAction func cancelPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchPressed(_ sender: Any) {
        
        self.verticalConstraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func goPressed(_ sender: Any) {
        
        if fromLocation.text != "" || toLocation.text != "" {
            
            fromLocationText = fromLocation.text!
            toLocationText = toLocation.text!
            configureMap()
            
        } else {
            
            let alert = UIAlertController(title: "Please fill in either field.", message: "", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                alert.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func xSearchPressed(_ sender: Any) {
        
        self.verticalConstraint.constant = 550
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        self.view.endEditing(true)
        
    }
    
    @IBAction func xInfoWindowPressed(_ sender: Any) {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.customInfoWindow.alpha = 0
        }) { (true) in
            self.customInfoWindow.removeFromSuperview()
        }
        
    }
    
    @IBAction func detailsPressed(_ sender: Any) {
        
        UIView.animate(withDuration: 0.1, animations: {
            self.customInfoWindow.alpha = 0
        }) { (true) in
            self.customInfoWindow.removeFromSuperview()
        }
        
        performSegue(withIdentifier: "showDetails", sender: nil)
        
    }
    
}
