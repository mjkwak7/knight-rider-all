//
//  VehicleCell.swift
//  Knight Rider
//
//  Created by Michael K. on 8/4/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit

class VehicleCell: UITableViewCell {

    @IBOutlet weak var vehicleName: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var make: UILabel!
    @IBOutlet weak var model: UILabel!
    @IBOutlet weak var seatsAvailable: UILabel!
    
    func configureCell(car: Car) {
        vehicleName.text = "Vehicle"
        make.text = car.maker.capitalized
        model.text = car.type.capitalized
        seatsAvailable.text = "\(car.capacity)"
    }
    
}
