//
//  AddCarVC.swift
//  Knight Rider
//
//  Created by Michael K. on 8/4/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit
import Alamofire
import SwiftKeychainWrapper

class AddCarVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var make: UITextField!
    @IBOutlet weak var model: UITextField!
    @IBOutlet weak var availableSeats: UITextField!
    
    let UID = KeychainWrapper.standard.string(forKey: UID_KEY)
    let TOKEN = KeychainWrapper.standard.string(forKey: TOKEN_KEY)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        make.delegate = self
        model.delegate = self
        availableSeats.delegate = self
        
        self.hideKeyboardWhenTappedAround()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }

    @IBAction func addCarPressed(_ sender: Any) {
        
        if make.text != "" && model.text != "" && availableSeats.text != "" {
            
            let parameters: Parameters = [
                "maker" : make.text!,
                "type" : model.text!,
                "capacity" : availableSeats.text!
            ]
            
            let tokenHeaders: HTTPHeaders = [
                "Content-Type": "application/json",
                "X-Authorization": "Bearer \(TOKEN!)",
                "Cache-Control": "no-cache"
            ]
            
            Alamofire.request(CARS_URL + "\(UID!)/cars", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { response in
                
                if let dict = response.result.value as? Dictionary<String, AnyObject> {
                    
                    if let make = dict["maker"] as? String {
                        
                        if make == self.make.text {
                            
                            self.dismiss(animated: true, completion: nil)
                            
                        }
                        
                    } else {
                        
                        let alert = UIAlertController(title: "Server error. Please try again later.", message: "", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        make.text = ""
        model.text = ""
        availableSeats.text = ""
        dismiss(animated: true, completion: nil)
    }
    
}
