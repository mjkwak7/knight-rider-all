//
//  RideDetailVC.swift
//  Knight Rider
//
//  Created by Michael K. on 7/21/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import CoreLocation
import SwiftKeychainWrapper

class RideDetailVC: UIViewController {
    
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var originAddress: UILabel!
    @IBOutlet weak var departureDateAndTime: UILabel!
    @IBOutlet weak var destinationAddress: UILabel!
    @IBOutlet weak var car: UILabel!
    @IBOutlet weak var remainingSeats: UILabel!
    @IBOutlet weak var messages: UILabel!
    @IBOutlet weak var driverPicture: UIImageView!
    @IBOutlet weak var driverFirstName: UILabel!
    @IBOutlet weak var driverLastName: UILabel!

    @IBOutlet weak var deleteConstraint: NSLayoutConstraint!
    @IBOutlet weak var leaveConstraint: NSLayoutConstraint!
    @IBOutlet weak var joinConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mapWindow: GMSMapView!
    
    var ride: Ride!
    var coordinate = CLLocationCoordinate2D()
    let coder = CLGeocoder()
    let UID = KeychainWrapper.standard.string(forKey: UID_KEY)
    let TOKEN = KeychainWrapper.standard.string(forKey: TOKEN_KEY)
    let ADDRESS = KeychainWrapper.standard.string(forKey: ADDRESS_KEY)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if ride?.driverId == Int(UID!) {
            
            deleteConstraint.constant = 0
            leaveConstraint.constant = 100
            joinConstraint.constant = 100
            
        } else {
            
            for passenger in ride.passengers {
                if passenger.id == Int(UID!) {
                    deleteConstraint.constant = 100
                    leaveConstraint.constant = 0
                    joinConstraint.constant = 100
                    break
                } else {
                    deleteConstraint.constant = 100
                    leaveConstraint.constant = 100
                    joinConstraint.constant = 0
                }
                
            }
            
        }
        
        loadDetails()
        loadMap()
        convertAddress()
    }
    
    func loadDetails() {
        origin.text = ride.originCity
        destination.text = ride.destCity
        originAddress.text = ride.originAddress
        destinationAddress.text = ride.destAddress
        departureDateAndTime.text = ride.departureTime
        car.text = ride.car.maker + " " + ride.car.type
        remainingSeats.text = "0" + "\(ride.remainingSeats)"
        messages.text = "0" + "\(ride.messages.count)"
        driverFirstName.text = ride.driver.firstName
        driverLastName.text = ride.driver.lastName
        
        if ride.driver.profilePicture != "" {
            let urlRequest = ride.driver.profilePicture
            Alamofire.request(urlRequest).responseImage { (response) in
                
                if let image = response.result.value {
                    self.driverPicture.image = image
                }
            }
            
        }
    }
    
    func loadMap() {
        
        self.mapWindow.camera = GMSCameraPosition.camera(withLatitude: ride.originLatitude, longitude: ride.originLongitude, zoom: 9)
        
        let marker1 = GMSMarker()
        marker1.position = CLLocationCoordinate2DMake(ride.originLatitude, ride.originLongitude)
        marker1.title = "\(ride.originCity)"
        marker1.map = mapWindow
        
        let marker2 = GMSMarker()
        marker2.position = CLLocationCoordinate2DMake(ride.destLatitude, ride.destLongitude)
        marker2.title = "\(ride.destCity)"
        marker2.map = mapWindow
        
        let path = GMSMutablePath()
        path.addLatitude(ride.originLatitude, longitude: ride.originLongitude)
        path.addLatitude(ride.destLatitude, longitude: ride.destLongitude)
        
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = .purple
        polyline.strokeWidth = 5.0
        polyline.map = mapWindow
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToMessages" {
            
            let nextVC = segue.destination as? MessageVC
            
            nextVC?.ride = self.ride
            
        }
        
    }
    
    func convertAddress() {
        coder.geocodeAddressString(ADDRESS!) { (response, error) in
            let latitude = (response?[0].location?.coordinate.latitude)!
            let longitude = (response?[0].location?.coordinate.longitude)!
            self.coordinate.latitude = latitude
            self.coordinate.longitude = longitude
        }
    
    }
    
    @IBAction func deletePressed(_ sender: Any) {
        
        let tokenHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Authorization": "Bearer \(TOKEN!)",
            "Cache-Control": "no-cache"
        ]
        
        Alamofire.request(DELETE_TRIP_URL + "\(ride.id)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { response in
            
            if let dict = response.result.value as? Dictionary<String, AnyObject> {
                
                if let message = dict["message"] as? String {
                    
                    if message == "Successfully deleted!" {
                        
                        self.dismiss(animated: true, completion: nil)
                        
                    }
                    
                } else {
                    
                    let alert = UIAlertController(title: "Server error. Please try again later.", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            }
            
        }
        
    }
    
    @IBAction func messagesPressed1(_ sender: Any) {
        performSegue(withIdentifier: "goToMessages", sender: nil)
    }
    
    @IBAction func messagesPressed2(_ sender: Any) {
        performSegue(withIdentifier: "goToMessages", sender: nil)
    }
    
    @IBAction func joinPressed(_ sender: Any) {
        print(coordinate.latitude)
        
        let parameters: Parameters = [
            "address": ADDRESS!,
            "latitude": self.coordinate.latitude,
            "longitude": self.coordinate.longitude,
            "userId": UID!,
            "tripId": ride.id
        ]
        
        let tokenHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Authorization": "Bearer \(TOKEN!)",
            "Cache-Control": "no-cache"
        ]
        
        Alamofire.request(PASSENGERS_URL + "\(ride.id)/\(UID!)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { response in
            
            print(response.debugDescription)
            
            if let dict = response.result.value as? Dictionary<String, AnyObject> {
                
                if let message = dict["message"] as? String {
                    
                    if message == "Successfully added!" {
                        
                        self.joinConstraint.constant = 100
                        self.leaveConstraint.constant = 0
                        UIView.animate(withDuration: 0.5, animations: {
                            self.view.layoutIfNeeded()
                        })
                    
                    }
                    
                } else {
                    
                    let alert = UIAlertController(title: "Server error. Please try again later.", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            }
            
        }
        
    }

    @IBAction func leavePressed(_ sender: Any) {
        
        let tokenHeaders: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Authorization": "Bearer \(TOKEN!)",
            "Cache-Control": "no-cache"
        ]
        
        Alamofire.request(PASSENGERS_URL + "\(ride.id)/\(UID!)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: tokenHeaders).responseJSON { (response) in
            
            if let dict = response.result.value as? Dictionary<String, AnyObject> {
                
                if let message = dict["message"] as? String {
                    
                    if message == "Successfully deleted!" {
                    
                        self.joinConstraint.constant = 0
                        self.leaveConstraint.constant = 100
                        UIView.animate(withDuration: 0.5, animations: {
                            self.view.layoutIfNeeded()
                        })
                        
                    }
                    
                } else {
                    
                    let alert = UIAlertController(title: "Server error. Please try again later.", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            }
            
        }
        
    }

}
