//
//  RideCell.swift
//  Knight Rider
//
//  Created by Michael K. on 7/25/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class RideCell: UITableViewCell {
    
    @IBOutlet weak var originCity: UILabel!
    @IBOutlet weak var destinationCity: UILabel!
    @IBOutlet weak var seatsAvailable: UILabel!
    @IBOutlet weak var departureTime: UILabel!
    @IBOutlet weak var originAddress: UILabel!
    @IBOutlet weak var destinationAddress: UILabel!
    @IBOutlet weak var messages: UILabel!
    @IBOutlet weak var profilePicture: CircleView!
    
    func configureCell(ride: Ride) {
        originCity.text = "\(ride.originCity)"
        destinationCity.text = "\(ride.destCity)"
        originAddress.text = "\(ride.originAddress)"
        departureTime.text = "\(ride.departureTime)"
        destinationAddress.text = "\(ride.destAddress)"
        seatsAvailable.text = "0" + "\(ride.remainingSeats)"
        messages.text = "0" + "\(ride.messages.count)"
        
        if ride.driver.profilePicture != "" {
            
            let urlRequest = URL(string: ride.driver.profilePicture)
            
            Alamofire.request(urlRequest!).responseImage { (response) in
                if let image = response.result.value {
                    self.profilePicture.image = image
                }
            }
            
        } else {
            
            self.profilePicture.image = UIImage()
            
        }
        
    }
    
}
