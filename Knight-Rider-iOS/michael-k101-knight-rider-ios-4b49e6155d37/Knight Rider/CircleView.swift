//
//  CircleView.swift
//  Knight Rider
//
//  Created by Michael K. on 8/2/17.
//  Copyright © 2017 MGA. All rights reserved.
//

import UIKit

class CircleView: UIImageView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = self.frame.width / 2
        layer.borderWidth = 2
        layer.borderColor = UIColor(red: 66/255, green: 183/255, blue: 122/255, alpha: 1).cgColor
    }
    
}
