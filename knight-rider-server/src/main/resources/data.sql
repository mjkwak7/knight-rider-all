insert into app_user(password, username, firstname, lastname, address, zip, phone, enabled) 
values('$2a$10$HM114MuN/p0VY7EeWqhIW.YmCi6ZfqO6SlXwCrj7W9QiJFztXr8gi', 'admin@mga.edu', 'John', 'Doe', '100 University Parkway', '31206', '478-471-2801', true);
insert into user_role(user_id, role) values(1, 'ADMIN');
insert into user_role(user_id, role) values(1, 'PREMIUM_MEMBER');

insert into car(user_id, car_maker, car_type, seats) values(1, 'Kia', 'Optima', 4);

insert into app_user(password, username, firstname, lastname, address, zip, phone, enabled) 
values('$2a$10$HM114MuN/p0VY7EeWqhIW.YmCi6ZfqO6SlXwCrj7W9QiJFztXr8gi', 'mike.smith@mga.edu', 'Mike', 'Smith', '100 University Parkway', '31206', '478-471-2801', TRUE);
insert into user_role(user_id, role) values(2, 'MEMBER');

insert into car(user_id, car_maker, car_type, seats) values(2, 'Ford', 'Escape', 4);


insert into trip(user_id, car_id, origin_address, origin_city, origin_latitude, origin_longitude, dest_address, dest_city, dest_latitude, dest_longitude, dept_time, meet_location, meet_latitude, meet_longitude, available_seats, remaining_seats) values(1, 1, '100 Univeristy Parkway, Macon, GA 31206', 'Macon', 32.808331, -83.734598, '100 University Boulevard, Warner Robins, GA 31093', 'Warner Robins',  32.617414, -83.608868, '2017-07-30 15:30:00.0', 'Macon',  32.808286, -83.734994,  4, 3);
insert into passenger(trip_id, user_id, join_date) values (1, 2, '2017-07-31 15:30:00.0');

insert into trip(user_id, car_id, origin_address, origin_city, origin_latitude, origin_longitude, dest_address, dest_city, dest_latitude, dest_longitude, dept_time, meet_location, meet_latitude, meet_longitude, available_seats, remaining_seats) values(2, 2, '100 University Boulevard, Warner Robins, GA 31093', 'Warner Robins',  32.617414, -83.608868, '100 Univeristy Parkway, Macon, GA 31206', 'Macon', 32.808331, -83.734598, '2017-08-05 15:30:00.0', 'Warner Robins',  32.617414, -83.608868,  4, 3);
insert into passenger(trip_id, user_id, join_date) values (2, 1, '2017-07-31 15:30:00.0');


insert into message(log_date, comment, trip_id, user_id) values ('2017-07-31 15:30:00.0', 'Hello, I just created a trip.', 1, 1);
insert into message(log_date, comment, trip_id, user_id) values ('2017-07-31 16:30:00.0', 'Hello, I would like to join this trip.', 1, 2);

insert into message(log_date, comment, trip_id, user_id) values ('2017-07-31 15:30:00.0', 'Hello, I just created a trip.', 2, 2);
insert into message(log_date, comment, trip_id, user_id) values ('2017-07-31 16:30:00.0', 'Hello, I would like to join this trip.', 2, 1);