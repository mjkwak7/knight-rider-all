package edu.mga.knightrider.validation;

import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

public class ValidationErrorBuilder {

	public static ValidationError fromBindingErrors(Errors errors) {
        ValidationError error = new ValidationError("Validation failed. " + errors.getErrorCount() + " error(s)");
        for (ObjectError objectError : errors.getAllErrors()) {
            error.addValidationError(objectError.getDefaultMessage());
        }
        return error;
    }
	
	public static ValidationError fromErrorMessages(List<String> errors) {
        ValidationError error = new ValidationError("Validation failed. " + errors.size() + " error(s)");
        for (String s : errors) {
            error.addValidationError(s);
        }
        return error;
    }
}
