package edu.mga.knightrider.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.mga.knightrider.exception.UserMismatchException;
import edu.mga.knightrider.model.User;
import edu.mga.knightrider.model.UserProfile;
import edu.mga.knightrider.model.viewmodel.ProfileViewModel;
import edu.mga.knightrider.model.viewmodel.UserViewModel;
import edu.mga.knightrider.model.viewmodel.ViewModelHelper;
import edu.mga.knightrider.repository.UserRepository;
import edu.mga.knightrider.security.auth.JwtAuthenticationToken;
import edu.mga.knightrider.security.model.UserContext;
import edu.mga.knightrider.security.service.UserSecurityService;
import edu.mga.knightrider.service.UserProfileService;
import edu.mga.knightrider.service.UserService;
import edu.mga.knightrider.util.GenericResponse;
import edu.mga.knightrider.util.ImageUtil;

@CrossOrigin
@RestController
@RequestMapping("/knightrider")
public class UserRestController {

	private static Logger logger = LoggerFactory.getLogger(UserRestController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserProfileService userProfileService;
	
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ViewModelHelper viewModelHelper;
	
	@Autowired
	private UserSecurityService userSecurityService;
	
	@Autowired
	private ImageUtil imageUtil;
	
	@RequestMapping(method = RequestMethod.GET, value="/users")
	public List<UserViewModel> getAllUsers(Authentication authentication){
		logger.info("userService.getAllUsers() ---");
		logger.info("request user:" + authentication.getName());
		UserContext context = (UserContext)((JwtAuthenticationToken)authentication).getPrincipal();
		logger.info("authentication user details:" +  context.getUsername());
		
		
		List<User> users = userService.getAllUsers();
		List<UserViewModel> userViewModels = new ArrayList<UserViewModel>();
		for (User u : users){
			UserViewModel user = viewModelHelper.getUserViewModelFromUser(u);
			userViewModels.add(user);
		}
		
		return userViewModels;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/users/{id}")
	public UserViewModel getUser(@PathVariable Long id) {
		logger.info("userService.getUser("+id+") ---");
		
		User u = userService.getUser(id);	
		
		logger.info("getting UserViewModel("+id+") ---");
		
		UserViewModel user = viewModelHelper.getUserViewModelFromUser(u);		
		return user;
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/users/{id}", consumes= MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public UserViewModel updateUser(@Valid @RequestBody UserViewModel user, @PathVariable Long id, 
			Authentication authentication) {
		logger.info("updateUser: "+id);
		logger.info("User to be updated: "+user);
		
		User u = userRepository.findOne(id); // userService.getUser(id);
		
		UserContext context = (UserContext)((JwtAuthenticationToken)authentication).getPrincipal();
		logger.info("usercontext:  " + context.getUsername());
		if (!context.getUsername().equals(u.getUsername())){
			throw new UserMismatchException("User Mismatch Exception!");
		}
		
		logger.info("Existing User: " +u);
		
		u.setFirstName(user.getFirstName());
		u.setLastName(user.getLastName());
		u.setAddress(user.getAddress());
		u.setZip(user.getZip());
		u.setPhone(user.getPhone());
		
		User savedUser = userService.updateUser(id, u);
		logger.info("Saved User: " +savedUser);
		
		return viewModelHelper.getUserViewModelFromUser(savedUser);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/users/profilepicture/{userId}", consumes= MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public UserViewModel uploadProfilePicture(@Valid @RequestBody ProfileViewModel model, @PathVariable Long userId,
			Authentication authentication) {		
		
		logger.info("uploading image: " + userId);
		//User u = userRepository.findOne(userId);
		User user = userService.getUser(userId);
		UserContext context = (UserContext)((JwtAuthenticationToken)authentication).getPrincipal();
		if (!context.getUsername().equals(user.getUsername())){
			throw new UserMismatchException("User Mismatch Exception!");
		}
		
		//User user = userService.getUser(userId);
		UserProfile up = userProfileService.findOneByUserId(userId);
		
		if (up != null){
			// update profile
			UserProfile profile = userProfileService.findOne(up.getId());
			
			String filePath = imageUtil.saveBase64ImageString(model.getProfilePicture(), userId, model.getExtension());
			//profile.setProfilePicture(model.getProfilePicture());		
			
			profile.setProfilePicture(filePath);
			userProfileService.updateOne(profile);
		} else {
			//user.setProfilePicture(model.getProfilePicture());
			UserProfile profile = new UserProfile();
			profile.setUser(user);
			profile.setUsername(user.getUsername());
			
			//profile.setProfilePicture(model.getProfilePicture());	
			String filePath = imageUtil.saveBase64ImageString(model.getProfilePicture(), userId, model.getExtension());
			profile.setProfilePicture(filePath);
			userProfileService.insertOne(profile);
		}
		
		User updatedUser = userService.getUser(userId);		
		
		return viewModelHelper.getUserViewModelFromUser(updatedUser);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/users/{id}")
	public ResponseEntity<Object> deleteUser(@PathVariable Long id, Authentication authentication) {
		logger.info("deleteUser: "+id);
		User user = userService.getUser(id);		
		//user.setEnabled(false);
		
		UserContext context = (UserContext)((JwtAuthenticationToken)authentication).getPrincipal();
		if (!context.getUsername().equals(user.getUsername())){
			throw new UserMismatchException("User Mismatch Exception!");
		}
		
		//userService.deleteUser(id);
		userSecurityService.deleteUser(user);
		return new ResponseEntity<Object>(new GenericResponse( "Successfully deleted!"), HttpStatus.OK);
	}
}
