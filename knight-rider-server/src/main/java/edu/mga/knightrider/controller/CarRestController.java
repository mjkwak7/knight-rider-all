package edu.mga.knightrider.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.mga.knightrider.exception.CarDeleteException;
import edu.mga.knightrider.exception.CarUpdateException;
import edu.mga.knightrider.exception.UserMismatchException;
import edu.mga.knightrider.model.Car;
import edu.mga.knightrider.model.Trip;
import edu.mga.knightrider.model.User;
import edu.mga.knightrider.model.viewmodel.CarViewModel;
import edu.mga.knightrider.security.auth.JwtAuthenticationToken;
import edu.mga.knightrider.security.model.UserContext;
import edu.mga.knightrider.service.CarService;
import edu.mga.knightrider.service.TripService;
import edu.mga.knightrider.service.UserService;
import edu.mga.knightrider.util.GenericResponse;

@CrossOrigin
@RestController
public class CarRestController {

	private static Logger logger = LoggerFactory.getLogger(CarRestController.class);
	
	@Autowired
	private CarService carService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TripService tripService;
	
	@RequestMapping(method = RequestMethod.GET, value="/knightrider/users/{userId}/cars")
	public List<Car> getAllCars(@PathVariable Long userId){
		logger.info("getAllCars(userId): "+userId);
		return carService.getAllCars(userId);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/knightrider/cars")
	public List<Car> getAllCars(){
		logger.info("getAllCars(): ");
		return carService.getAllCars();
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/knightrider/cars/{id}")
	public Car getCar(@PathVariable Long id) {
		logger.info("getCar(id): "+id);
		return carService.getCar(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/knightrider/users/{userId}/cars")
	public Car addCar(@PathVariable Long userId, @Valid @RequestBody CarViewModel model) {
		logger.info("addCar: "+userId);
		logger.info("addCar: "+model);
		
		Car car = new Car();
		car.setMaker(model.getMaker());
		car.setType(model.getType());
		car.setCapacity(model.getCapacity());
		
		return carService.addCar(userId, car);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/knightrider/users/{userId}/cars/{id}")
	public ResponseEntity<Object> updateCar(@Valid @RequestBody CarViewModel model, @PathVariable Long userId,  
			@PathVariable Long id, Authentication authentication) {
		
		logger.info("updateCar: "+id);
		logger.info("updateCar: "+model);
		logger.info("updateCar userid: "+userId);
		
		UserContext context = (UserContext)((JwtAuthenticationToken)authentication).getPrincipal();
		if (!context.getUserId().equals(userId)){
			throw new UserMismatchException("User Mismatch Exception!");
		}
		
		Car car = carService.getCar(id);
		
		if (car == null){
			throw new CarUpdateException("Car doesn't exist.");
		}
		
		User user = userService.getUser(userId);
		if (user.getCars() == null || !own(user, car)){
			throw new CarUpdateException("This car doesn't belong to you.");
		}
		
		car.setMaker(model.getMaker());
		car.setType(model.getType());
		car.setCapacity(model.getCapacity());
		try {
			carService.updateCar(id, userId, car);
		} catch (Exception ex){
			throw new CarUpdateException("This car cannot be deleted.");
		}

		return new ResponseEntity<Object>(new GenericResponse( "Successfully updated!"), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/knightrider/cars/{id}")
	public ResponseEntity<Object> deleteCar(@PathVariable Long id, Authentication authentication) {
		logger.info("deleteCar: "+id);
		
		Car car = carService.getCar(id);
		if (car == null ){
			throw new CarDeleteException("This car doesn't exist.");
		}
		
		List<Trip> trips = tripService.getTripsByCarId(id);
		if (trips == null || trips.size() > 0){
			throw new CarDeleteException("This car cannot be deleted. It is used by trip(s).");
		}
		
		UserContext context = (UserContext)((JwtAuthenticationToken)authentication).getPrincipal();		
		Long userId = context.getUserId();
		User user = userService.getUser(userId);
		if (user.getCars() == null || !own(user, car)){
			throw new CarDeleteException("This car doesn't belong to you.");
		}		
		
		try{
			carService.deleteCar(id);
		} catch (Exception ex){
			throw new CarDeleteException("Cannot delete this car.", ex.getCause());
		}
		
		return new ResponseEntity<Object>(new GenericResponse( "Successfully deleted!"), HttpStatus.OK);
	}
	
	private boolean own(User user, Car car){
		for (Car c : user.getCars()){
			if (c.getId().equals(car.getId()))
				return true;
		}
		return false;
	}
}
