package edu.mga.knightrider.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.mga.knightrider.exception.TripAddNoCarInfoException;
import edu.mga.knightrider.exception.TripDeleteException;
import edu.mga.knightrider.exception.TripUpdateException;
import edu.mga.knightrider.exception.UserMismatchException;
import edu.mga.knightrider.model.Car;
import edu.mga.knightrider.model.Trip;
import edu.mga.knightrider.model.User;
import edu.mga.knightrider.model.viewmodel.TripSearchViewModel;
import edu.mga.knightrider.model.viewmodel.TripViewModel;
import edu.mga.knightrider.model.viewmodel.ViewModelHelper;
import edu.mga.knightrider.security.auth.JwtAuthenticationToken;
import edu.mga.knightrider.security.model.UserContext;
import edu.mga.knightrider.service.CarService;
import edu.mga.knightrider.service.TripService;
import edu.mga.knightrider.service.UserService;
import edu.mga.knightrider.util.GenericResponse;

@CrossOrigin
@RestController
public class TripRestController {
	
	private static Logger logger = LoggerFactory.getLogger(TripRestController.class);
	
	@Autowired
	private TripService tripService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CarService carService;
	
	@Autowired
	private ViewModelHelper viewModelHelper;
	
		
	@RequestMapping(method=RequestMethod.GET, value="/knightrider/drivers/{userId}/trips")
	public List<TripViewModel> getAllTripsForDriver(@PathVariable Long userId){
		
		logger.info("getAllTrips(userId): "+userId);
		List<Trip> trips = tripService.getAllTripsForDriver(userId);
		User driver = userService.getUser(userId);
		
		List<TripViewModel> tripViewList = new ArrayList<TripViewModel>();
		for (Trip trip : trips){
			tripViewList.add(viewModelHelper.getTripViewModelFromTripAndUser(trip, driver));
		}		
		
		return tripViewList;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/knightrider/users/{userId}/trips")
	public List<TripViewModel> getAllTripsForDriversAndPassengers(@PathVariable Long userId){
		
		logger.info("getAllTrips(userId): "+userId);
		List<Trip> trips = tripService.getAllTripsForDriverAndPassenger(userId);//.getAllTrips(userId);
		//User user = userService.getUser(userId);
		
		List<TripViewModel> tripViewList = new ArrayList<TripViewModel>();
		for (Trip trip : trips){
			tripViewList.add(viewModelHelper.getTripViewModelFromTrip(trip));
		}
		return tripViewList;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/knightrider/trips")
	public List<TripViewModel> getAllTrips(){
		logger.info("getAllTrips(): ");
		
		List<Trip> trips = tripService.getAllTrips();		
		List<TripViewModel> tripViewList = new ArrayList<TripViewModel>();
		for (Trip t : trips){
			tripViewList.add(viewModelHelper.getTripViewModelFromTrip(t));
		}				
		
		return tripViewList;
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/knightrider/trips/search")
	public List<TripViewModel> searchAllTrips(@RequestBody TripSearchViewModel model){
		logger.info("getAllTrips(): ");
		
		List<Trip> trips = tripService.searchTrips(model);		
		
		List<TripViewModel> tripViewList = new ArrayList<TripViewModel>();
		for (Trip t : trips){
			tripViewList.add(viewModelHelper.getTripViewModelFromTrip(t));
		}				
		
		return tripViewList;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/knightrider/trips/{id}")
	public TripViewModel getTrip(@PathVariable Long id) {
		logger.info("getTrip(): "+id);		
		return viewModelHelper.getTripViewModel(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/knightrider/users/{userId}/trips")
	public ResponseEntity<Object> addTrip(@PathVariable Long userId, @Valid @RequestBody TripViewModel model, Authentication authentication) {
		logger.info("addTrip(userId, trip): "+userId);		
		logger.info("addTrip(userId, trip): "+model);
		
		UserContext context = (UserContext)((JwtAuthenticationToken)authentication).getPrincipal();
		if (!context.getUserId().equals(userId)){
			throw new UserMismatchException("Requesting user information is invalid.");
		}
		
		if (model.getCarId() == null){
			throw new TripAddNoCarInfoException("Please insert your car information.");
		}
		
		Trip newTrip = viewModelHelper.getTripFromUserIdAndTripViewModel(userId, model);
		
		// set the remaining seats as the same as available seats
		newTrip.setRemainingSeats(newTrip.getAvailableSeats());
		
		try {
			Trip addedTrip = tripService.addTrip(userId, newTrip);		
			logger.info("addedTrip: "+addedTrip);
		} catch (Exception ex){
			throw new RuntimeException("Cannot create this trip!");
		}
		
		//return viewModelHelper.getTripViewModelFromTrip(addedTrip);
		return new ResponseEntity<Object>(new GenericResponse( "Successfully created!"), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/knightrider/users/{userId}/trips/{id}")
	public ResponseEntity<Object> updateTrip(@Valid @RequestBody TripViewModel model, @PathVariable Long userId, 
			@PathVariable Long id, Authentication authentication) {
		
		logger.info("updateTrip(id, trip, userId): "+id);
		logger.info("updateTrip(id, trip, userId): "+model);
		logger.info("updateTrip(id, trip, userId): "+userId);

		Trip trip = tripService.getTrip(id);// getTrip(userId, tripViewModel);
		if (trip == null){
			throw new TripUpdateException("Trip doesn't exist!");
		}		

		UserContext context = (UserContext)((JwtAuthenticationToken)authentication).getPrincipal();
		if (!context.getUserId().equals(userId)){
			throw new UserMismatchException("Requesting user information is invalid.");
		}
		
		
		if (model.getCarId() != trip.getCar().getId()){			
			Car car = carService.getCar(model.getCarId());
			trip.setCar(car);
			
			logger.info("Updating car information: " + car);
		}
			
		// update trip fields with view model
		trip.setOriginAddress(model.getOriginAddress());
		trip.setOriginCity(model.getOriginCity());
		trip.setOriginState(model.getOriginState());
		trip.setOriginZip(model.getOriginZip());
		trip.setOriginLatitude(model.getOriginLatitude());
		trip.setOriginLongitude(model.getOriginLongitude());
		
		trip.setDestAddress(model.getDestAddress());
		trip.setDestCity(model.getDestCity());
		trip.setDestState(model.getDestState());
		trip.setDestZip(model.getDestZip());
		trip.setDestLatitude(model.getDestLatitude());
		trip.setDestLongitude(model.getDestLongitude());
		
		trip.setDepartureTime(model.getDepartureTime());
		
		trip.setMeetingLocation(model.getMeetingLocation());
		trip.setMeetingLatitude(model.getMeetingLatitude());
		trip.setMeetingLongitude(model.getMeetingLongitude());
		trip.setAvailableSeats(model.getAvailableSeats());
		
		// 
		Integer newRemainingSeats = model.getAvailableSeats() - trip.getPassengers().size();
		trip.setRemainingSeats(newRemainingSeats);
		
		if(newRemainingSeats<0){
			// 
			logger.info("# of remaining seats " + newRemainingSeats);
			throw new TripUpdateException("Invalid remaining seats. Please ask a passenger to leave this trip and try again!");
		}
		
		try {
			tripService.updateTrip(id, userId, trip);
		} catch(Exception ex){
			throw new TripUpdateException("Internal Server Error");
		}
		
		//return viewModelHelper.getTripViewModelFromTrip(updatedTrip);
		return new ResponseEntity<Object>(new GenericResponse( "Successfully updated!"), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/knightrider/trips/{id}")
	public ResponseEntity<Object> deleteTrip(@PathVariable Long id, Authentication authentication) {
		logger.info("deleteTrip(id): "+id);
		
		Trip trip = tripService.getTrip(id);
		
		UserContext context = (UserContext)((JwtAuthenticationToken)authentication).getPrincipal();
		if (trip == null || !context.getUserId().equals(trip.getUser().getId())){
			throw new UserMismatchException("Requesting user information is invalid.");
		}
		
		try {						
			tripService.deleteTrip(id);		
		} catch (Exception e){
			throw new TripDeleteException("Cannot delete this trip!");
		}	
		
		return new ResponseEntity<Object>(new GenericResponse( "Successfully deleted!"), HttpStatus.OK);
	}
	
}
