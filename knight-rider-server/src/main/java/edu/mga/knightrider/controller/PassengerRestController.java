package edu.mga.knightrider.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.mga.knightrider.exception.TripJoinException;
import edu.mga.knightrider.exception.TripNotFoundException;
import edu.mga.knightrider.model.Passenger;
import edu.mga.knightrider.model.Trip;
import edu.mga.knightrider.model.viewmodel.PassengerViewModel;
import edu.mga.knightrider.model.viewmodel.ViewModelHelper;
import edu.mga.knightrider.service.PassengerService;
import edu.mga.knightrider.service.TripService;
import edu.mga.knightrider.util.GenericResponse;

@CrossOrigin
@RestController
public class PassengerRestController {

	private static Logger logger = LoggerFactory.getLogger(PassengerRestController.class);
	
	@Autowired
	private PassengerService passengerService;
	
	@Autowired
	private TripService tripService;
	
	@Autowired
	private ViewModelHelper viewModelHelper;
	
	@RequestMapping(method=RequestMethod.GET, value="/knightrider/passengers/{tripId}")
	public List<PassengerViewModel> getAllPassengersByTripId(@PathVariable Long tripId){
		logger.info("getAllPassengersByTripId(tripId): "+tripId);
		
		
		List<Passenger> passengers = passengerService.getAllPassengersByTripId(tripId);
		
		List<PassengerViewModel> models = new ArrayList<PassengerViewModel>();
		for (Passenger p : passengers){
			models.add(viewModelHelper.getPassengerViewModelFromPassenger(p));
		}
		
		return models;	
	}
	
	
	@RequestMapping(method=RequestMethod.POST, value="/knightrider/passengers/{tripId}/{userId}")
	public ResponseEntity<Object> addPassenger(@PathVariable Long tripId, @PathVariable Long userId) {
		logger.info("adding Passenger(tripId, userId): " + tripId + ", " +userId);
		
		Trip trip = tripService.getTrip(tripId);
		
		if (trip == null)
			throw new TripNotFoundException("Trip not found!");
		
		if (trip.getUser().getId() == userId){
			throw new TripJoinException("Driver cannot join as a passenger");
		}
		
		if (trip.getRemainingSeats() == 0){
			throw new TripJoinException("There is no remaining seat.");
		}
		
		if (passengerService.exist(tripId, userId)){
			throw new TripJoinException("You already joined this trip.");
		}
		
		logger.info("adding Passenger using passengerService: " + tripId + ", " +userId);		
		
		try {
		
			passengerService.addPassenger(tripId, userId);
			
		} catch(RuntimeException ex){
			throw new TripJoinException("Cannot join as a passenger");
		}
		
		return new ResponseEntity<Object>(new GenericResponse( "Successfully added!"), HttpStatus.OK);
		
	}
	
	
	
	@RequestMapping(method=RequestMethod.DELETE, value="/knightrider/passengers/{tripId}/{userId}")
	public ResponseEntity<Object> deletePassenger(@PathVariable Long tripId, @PathVariable Long userId) {
		logger.info("deletePassenger(tripId, userId): " + tripId + ", " +userId);
		
		Trip trip = tripService.getTrip(tripId);
		if (trip == null){
			throw new TripNotFoundException("Trip not found!");
		}
		
		passengerService.deletePassenger(tripId, userId);
		
		return new ResponseEntity<Object>(new GenericResponse( "Successfully deleted!"), HttpStatus.OK);
	}

}
