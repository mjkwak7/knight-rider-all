package edu.mga.knightrider.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.mga.knightrider.model.Review;
import edu.mga.knightrider.model.viewmodel.ReviewViewModel;
import edu.mga.knightrider.model.viewmodel.ViewModelHelper;
import edu.mga.knightrider.service.ReviewService;
import edu.mga.knightrider.util.GenericResponse;

@CrossOrigin
@RestController
public class ReviewRestController {

	private static Logger logger = LoggerFactory.getLogger(ReviewRestController.class);

	@Autowired
	private ReviewService reviewService;

	//@Autowired
	//private UserService userService;

	@Autowired
	private ViewModelHelper viewModelHelper;

	@RequestMapping(method = RequestMethod.GET, value = "/knightrider/reviews/{tripId}")
	public List<ReviewViewModel> getAllReviewsByTripId(@PathVariable Long tripId) {
		logger.info("getAllReviewsByTripId(tripId): " + tripId);
		List<Review> reviews = reviewService.getAllReviewsByTripId(tripId);

		List<ReviewViewModel> models = new ArrayList<ReviewViewModel>();
		for (Review r : reviews) {
			models.add(viewModelHelper.getReviewViewModelFromReview(r));
		}
		return models;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/knightrider/reviews/{tripId}/{userId}")
	public ResponseEntity<Object> addReview(@PathVariable Long tripId, @PathVariable Long userId,
			@Valid @RequestBody ReviewViewModel model) {

		logger.info("addReview: " + tripId + ", " + userId);
		logger.info("review: " + model);

		Review review = reviewService.addReview(tripId, userId, model.getComment(), model.getScore());
		//User user = userService.getUser(userId);

		//return viewModelHelper.getReviewViewModelFromReview(review);
		return new ResponseEntity<Object>(new GenericResponse( "Successfully added!"), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/knightrider/review/{reviewId}")
	public ResponseEntity<Object> deleteMessage(@PathVariable Long reviewId) {

		logger.info("delete message: " + reviewId);

		reviewService.deleteByReviewId(reviewId);

		return new ResponseEntity<Object>(new GenericResponse( "Successfully deleted!"), HttpStatus.OK);
	}

}
