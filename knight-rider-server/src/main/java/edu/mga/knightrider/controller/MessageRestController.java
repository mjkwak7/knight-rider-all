package edu.mga.knightrider.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.mga.knightrider.model.Message;
import edu.mga.knightrider.model.viewmodel.MessageViewModel;
import edu.mga.knightrider.model.viewmodel.ViewModelHelper;
import edu.mga.knightrider.service.MessageService;
import edu.mga.knightrider.util.GenericResponse;

@CrossOrigin
@RestController
public class MessageRestController {

	private static Logger logger = LoggerFactory.getLogger(MessageRestController.class);

	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ViewModelHelper viewModelHelper;

	@RequestMapping(method = RequestMethod.GET, value = "/knightrider/messages/{tripId}")
	public List<MessageViewModel> getAllMessagesByTripId(@PathVariable Long tripId) {
		logger.info("getAllMessagesByTripId(tripId): " + tripId);
		List<Message> messages = messageService.getAllMessagesByTripId(tripId);
		
		List<MessageViewModel> models = new ArrayList<MessageViewModel>();
		for (Message m : messages){
			models.add(viewModelHelper.getMessageViewModelFromMessage(m));
		}
		
		return models;	
		
	}

	@RequestMapping(method = RequestMethod.POST, value = "/knightrider/messages/{tripId}/{userId}")
	public ResponseEntity<Object> addMessage(@PathVariable Long tripId, @PathVariable Long userId, @Valid @RequestBody MessageViewModel model) {

		logger.info("addMessage: " + tripId + ", " + userId);
		logger.info("message: " + model);

		Message message = messageService.addMessage(tripId, userId, model.getComment());
		
		//return viewModelHelper.getMessageViewModelFromMessage(message);
		return new ResponseEntity<Object>(new GenericResponse( "Successfully updated!"), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/knightrider/messages/{messageId}")
	public ResponseEntity<Object> deleteMessage(@PathVariable Long messageId) {

		logger.info("delete message: " + messageId);		
		messageService.deleteByMessageId(messageId);

		return new ResponseEntity<Object>(new GenericResponse( "Successfully deleted!"), HttpStatus.OK);
	}
}
