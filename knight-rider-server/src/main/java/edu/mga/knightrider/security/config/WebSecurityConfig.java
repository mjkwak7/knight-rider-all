package edu.mga.knightrider.security.config;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.mga.knightrider.controller.TripRestController;
import edu.mga.knightrider.security.RestAuthenticationEntryPoint;
import edu.mga.knightrider.security.auth.ajax.AjaxAuthenticationProvider;
import edu.mga.knightrider.security.auth.ajax.AjaxLoginProcessingFilter;
import edu.mga.knightrider.security.auth.jwt.JwtAuthenticationProvider;
import edu.mga.knightrider.security.auth.jwt.JwtTokenAuthenticationProcessingFilter;
import edu.mga.knightrider.security.auth.jwt.SkipPathRequestMatcher;
import edu.mga.knightrider.security.auth.jwt.extractor.TokenExtractor;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	private static Logger logger = LoggerFactory.getLogger(WebSecurityConfig.class);
	
    public static final String JWT_TOKEN_HEADER_PARAM = "X-Authorization";
    public static final String FORM_BASED_LOGIN_ENTRY_POINT = "/knightrider/auth/login";
    public static final String FORM_BASED_REGISTRATION_POINT = "/knightrider/auth/register";
    public static final String EMAIL_VALIDATION_POINT = "/knightrider/auth/registrationConfirm";
    public static final String TOKEN_BASED_AUTH_ENTRY_POINT = "/knightrider/**";
    public static final String TOKEN_REFRESH_ENTRY_POINT = "/knightrider/auth/token";
    public static final String EMAIL_VALIDATION_RESULT_POINT = "/knightrider/registrationconfirmresult";
    
    @Autowired private RestAuthenticationEntryPoint authenticationEntryPoint;
    @Autowired private AuthenticationSuccessHandler successHandler;
    @Autowired private AuthenticationFailureHandler failureHandler;
    @Autowired private AjaxAuthenticationProvider ajaxAuthenticationProvider;
    @Autowired private JwtAuthenticationProvider jwtAuthenticationProvider;
    
    @Autowired private TokenExtractor tokenExtractor;
    
    @Autowired private AuthenticationManager authenticationManager;
    
    @Autowired private ObjectMapper objectMapper;
      
    
    protected AjaxLoginProcessingFilter buildAjaxLoginProcessingFilter() throws Exception {
    	
    	if(logger.isDebugEnabled()) {
            logger.debug("buildAjaxLoginProcessingFilter");
        }
        AjaxLoginProcessingFilter filter = new AjaxLoginProcessingFilter(FORM_BASED_LOGIN_ENTRY_POINT, successHandler, failureHandler, objectMapper);
        filter.setAuthenticationManager(this.authenticationManager);
        return filter;
    }
    
    
    protected JwtTokenAuthenticationProcessingFilter buildJwtTokenAuthenticationProcessingFilter() throws Exception {
    	if(logger.isDebugEnabled()) {
            logger.debug("buildJwtTokenAuthenticationProcessingFilter");
        }
        List<String> pathsToSkip = Arrays.asList(TOKEN_REFRESH_ENTRY_POINT, FORM_BASED_LOGIN_ENTRY_POINT, 
        		FORM_BASED_REGISTRATION_POINT, EMAIL_VALIDATION_POINT, EMAIL_VALIDATION_RESULT_POINT);
        SkipPathRequestMatcher matcher = new SkipPathRequestMatcher(pathsToSkip, TOKEN_BASED_AUTH_ENTRY_POINT);
        JwtTokenAuthenticationProcessingFilter filter 
            = new JwtTokenAuthenticationProcessingFilter(failureHandler, tokenExtractor, matcher);
        filter.setAuthenticationManager(this.authenticationManager);
        return filter;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
    	if(logger.isDebugEnabled()) {
            logger.debug("authenticationManagerBean");
        }
        return super.authenticationManagerBean();
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
    	
    	if(logger.isDebugEnabled()) {
            logger.debug("configureconfigure(AuthenticationManagerBuilder auth)");
        }
    	
        auth.authenticationProvider(ajaxAuthenticationProvider);
        auth.authenticationProvider(jwtAuthenticationProvider);
    }
    
    //@Override
    //public void configure(WebSecurity web) throws Exception {
    //    web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
    //}
    
    @Bean
	CorsConfigurationSource corsConfigurationSource() {
    	
    	if(logger.isDebugEnabled()) {
            logger.debug("corsConfigurationSource");
        }
    	
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.addAllowedOrigin("*");
		configuration.addAllowedHeader("*");
		configuration.setAllowedMethods(Arrays.asList("GET","POST","PUT","DELETE", "OPTIONS"));
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	
    	if(logger.isDebugEnabled()) {
            logger.debug("configure(HttpSecurity http)");
        }
    	
        http
        .cors().and() // handle cors first
        .csrf().disable() // We don't need CSRF for JWT based authentication
        .exceptionHandling()
        .authenticationEntryPoint(this.authenticationEntryPoint)
        
        .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        .and()
            .authorizeRequests()
                .antMatchers(FORM_BASED_LOGIN_ENTRY_POINT).permitAll() // Login end-point
                .antMatchers(TOKEN_REFRESH_ENTRY_POINT).permitAll() // Token refresh end-point
                .antMatchers(FORM_BASED_REGISTRATION_POINT).permitAll() // registration end-point
                .antMatchers(EMAIL_VALIDATION_POINT).permitAll() // email validation point
                .antMatchers(EMAIL_VALIDATION_RESULT_POINT).permitAll() // email validation point
                .antMatchers("/console").permitAll() // H2 Console Dash-board - only for testing
        .and()
            .authorizeRequests()
                .antMatchers(TOKEN_BASED_AUTH_ENTRY_POINT).authenticated() // Protected API End-points
        .and()
            .addFilterBefore(buildAjaxLoginProcessingFilter(), UsernamePasswordAuthenticationFilter.class)
            .addFilterBefore(buildJwtTokenAuthenticationProcessingFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}
