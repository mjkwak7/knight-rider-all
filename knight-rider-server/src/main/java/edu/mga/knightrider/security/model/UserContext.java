package edu.mga.knightrider.security.model;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;

/**
 * 
 */
public class UserContext {
    private final Long userId;
    private final String username;
    private final List<GrantedAuthority> authorities;

    private UserContext(Long userId, String username, List<GrantedAuthority> authorities) {
        this.userId = userId;
        this.username = username;
        this.authorities = authorities;
    }
    
    public static UserContext create(Long userId, String username, List<GrantedAuthority> authorities) {
        if (StringUtils.isBlank(username)) throw new IllegalArgumentException("Username is blank: " + username);
        if (userId == null) throw new IllegalArgumentException("UserId is empty: " + userId);
        return new UserContext(userId, username, authorities);
    }

    public String getUsername() {
        return username;
    }
    
    public Long getUserId() {
		return userId;
	}

	public List<GrantedAuthority> getAuthorities() {
        return authorities;
    }

	@Override
	public String toString() {
		return "UserContext [userId=" + userId + ", username=" + username + ", authorities=" + authorities + "]";
	}

	
}
