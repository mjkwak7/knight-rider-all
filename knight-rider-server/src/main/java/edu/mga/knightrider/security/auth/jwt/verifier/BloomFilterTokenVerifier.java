package edu.mga.knightrider.security.auth.jwt.verifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class BloomFilterTokenVerifier implements TokenVerifier {
	
	private static Logger logger = LoggerFactory.getLogger(BloomFilterTokenVerifier.class);
	
    @Override
    public boolean verify(String jti) {
    	
    	if(logger.isDebugEnabled()) {
            logger.debug("Token to verify: " + jti);
        }
    	
        return true;
    }
}
