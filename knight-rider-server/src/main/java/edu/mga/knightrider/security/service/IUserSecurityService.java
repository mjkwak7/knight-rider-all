package edu.mga.knightrider.security.service;


import java.util.Optional;

import edu.mga.knightrider.exception.UserAlreadyExistException;
import edu.mga.knightrider.model.User;
import edu.mga.knightrider.security.dto.UserDto;
import edu.mga.knightrider.security.model.PasswordResetToken;
import edu.mga.knightrider.security.model.VerificationToken;



public interface IUserSecurityService {

    User registerNewUserAccount(UserDto accountDto) throws UserAlreadyExistException;

    User getUser(String verificationToken);

    void saveRegisteredUser(User user);
   
    void deleteUser(User user);

    void createVerificationTokenForUser(User user, String token);

    VerificationToken getVerificationToken(String VerificationToken);

    VerificationToken generateNewVerificationToken(String token);

    void createPasswordResetTokenForUser(User user, String token);

    Optional<User> findUserByEmail(String email);

    PasswordResetToken getPasswordResetToken(String token);

    User getUserByPasswordResetToken(String token);

    User getUserByID(long id);

    void changeUserPassword(User user, String password);

    boolean checkIfValidOldPassword(User user, String password);

    String validateVerificationToken(String token);


}
