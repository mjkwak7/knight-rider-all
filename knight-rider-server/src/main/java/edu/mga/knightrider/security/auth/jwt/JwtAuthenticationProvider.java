package edu.mga.knightrider.security.auth.jwt;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import edu.mga.knightrider.model.User;
import edu.mga.knightrider.security.auth.JwtAuthenticationToken;
import edu.mga.knightrider.security.config.JwtSettings;
import edu.mga.knightrider.security.model.UserContext;
import edu.mga.knightrider.security.model.token.JwtToken;
import edu.mga.knightrider.security.model.token.RawAccessJwtToken;
import edu.mga.knightrider.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

/**
 * An {@link AuthenticationProvider} implementation that will use provided
 * instance of {@link JwtToken} to perform authentication.
 * 
 */
@Component
@SuppressWarnings("unchecked")
public class JwtAuthenticationProvider implements AuthenticationProvider {
	
	private static Logger logger = LoggerFactory.getLogger(JwtAuthenticationProvider.class);
	
    private final JwtSettings jwtSettings;
    private final UserService userService;
    
    @Autowired
    public JwtAuthenticationProvider(JwtSettings jwtSettings,final UserService userService) {
        this.jwtSettings = jwtSettings;
        this.userService = userService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        RawAccessJwtToken rawAccessToken = (RawAccessJwtToken) authentication.getCredentials();

        Jws<Claims> jwsClaims = rawAccessToken.parseClaims(jwtSettings.getTokenSigningKey());
        String subject = jwsClaims.getBody().getSubject();
        List<String> scopes = jwsClaims.getBody().get("scopes", List.class);
        List<GrantedAuthority> authorities = scopes.stream()
                .map(authority -> new SimpleGrantedAuthority(authority))
                .collect(Collectors.toList());
        
        User user = userService.getByUsername(subject).orElseThrow(() -> new UsernameNotFoundException("User not found: " + subject));
        
        UserContext context = UserContext.create(user.getId(), subject, authorities);
        
        if(logger.isDebugEnabled()) {
            logger.debug("UserContext created: " + context.toString());
        }
        
        return new JwtAuthenticationToken(context, context.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
