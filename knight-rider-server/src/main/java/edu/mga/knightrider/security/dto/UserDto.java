package edu.mga.knightrider.security.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import edu.mga.knightrider.validation.PasswordMatches;
import edu.mga.knightrider.validation.ValidEmail;
import edu.mga.knightrider.validation.ValidPassword;



@PasswordMatches
public class UserDto {
    
	@NotBlank(message="FristName cannot be empty!")
	@NotNull
    @Size(min = 1)
    private String firstName;

	@NotBlank(message="LastName cannot be empty!")
    @NotNull
    @Size(min = 1)
    private String lastName;

	@NotBlank(message="Password cannot be empty!")
    @ValidPassword
    private String password;

    @NotBlank(message="Matching Password cannot be empty!")
    @NotNull
    @Size(min = 1)
    private String matchingPassword;

    @NotBlank(message="Email cannot be empty!")
    @ValidEmail
    @NotNull
    @Size(min = 1)
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    private Integer role;

    public Integer getRole() {
        return role;
    }

    public void setRole(final Integer role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(final String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("UserDto [firstName=").append(firstName).append(", lastName=")
        .append(lastName).append(", password=").append(password).append(", matchingPassword=")
        .append(matchingPassword).append(", email=").append(email).append(", role=")
        .append(role).append("]");
        
        return builder.toString();
    }

}
