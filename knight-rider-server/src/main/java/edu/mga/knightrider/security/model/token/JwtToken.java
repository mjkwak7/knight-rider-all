package edu.mga.knightrider.security.model.token;

public interface JwtToken {
    String getToken();
}
