package edu.mga.knightrider.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="passenger")
public class Passenger implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	
	@Embeddable
    public static class Id implements Serializable {
        private static final long serialVersionUID = 1L;
        
        @Column(name = "USER_ID", nullable = false)
        protected Long userId;
        
       
        @Column(name = "TRIP_ID", nullable = false)
        protected Long tripId;
        
        public Id() { }

        public Id(Long tripId, Long userId) {
            this.userId = userId;
            this.tripId = tripId;
        }
    }
	
	@EmbeddedId
    Id id = new Id();
    
    public void setId(Id id) {
		this.id = id;
	}
    
	
	@Column(name="JOIN_DATE")
	private Date joinDate;
	
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	
	@Column(name = "USER_ID", insertable=false, updatable=false)
    protected Long userId;

	@Column(name = "TRIP_ID", insertable=false, updatable=false)
    protected Long tripId;     
   
    public Long getUserId() {
		return userId;
	}
	public Long getTripId() {
		return tripId;
	}
      
	
	@Override
	public String toString() {
		return "Passenger [joinDate=" + joinDate + ", tripId=" + id.tripId + ", userId=" + id.userId + "]";
	}
	
	
}
