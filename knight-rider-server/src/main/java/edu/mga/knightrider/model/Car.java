package edu.mga.knightrider.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="car")
public class Car {
	
	
	@JsonIgnore
	@OneToMany (orphanRemoval = false) 
	@JoinColumn(name="car_id", referencedColumnName="ID")
	private List<Trip> trips;
		
		
	public List<Trip> getTrips() {
		return trips;
	}
	public void setTrips(List<Trip> trips) {
		this.trips = trips;
	}

	@JsonIgnore
	@ManyToOne 
	@JoinColumn(name="user_id", referencedColumnName="ID")
	private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	@Id @Column(name="id")
	@GeneratedValue
	private Long id;
	
	public Long getId() {
		return id;
	}
		
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="car_maker")
	private String maker;
	
	@Column(name="car_type")
	private String type;
	
	@Column(name="seats")
	private Integer capacity;

	public String getMaker() {
		return maker;
	}
	public void setMaker(String maker) {
		this.maker = maker;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getCapacity() {
		return capacity;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	public Car(User user, String maker, String type, Integer capacity) {
		super();
		this.user = user;
		this.maker = maker;
		this.type = type;
		this.capacity = capacity;
	}
	
	public Car(){}
	@Override
	public String toString() {
		return "Car [id=" + id + ",\n maker=" + maker + ",\n type=" + type + ",\n capacity=" + capacity + "]";
	}
	
	
}
