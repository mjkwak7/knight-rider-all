package edu.mga.knightrider.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="user_profile")
public class UserProfile {
	
	
	@JsonIgnore
	@OneToOne 
	@JoinColumn(name="user_id", referencedColumnName="id")
	private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	@Id @Column(name="id")
	@GeneratedValue
	private Long id;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
    @Column(name="username")
    private String username; 
    
	public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
		this.username = username;
	}
    
    @Lob
    @Column(name="profile_picture")
    private String profilePicture;
    
	public String getProfilePicture() {
		return profilePicture;
	}
	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}
	
	
	public UserProfile() {
		super();
	}
	
	public UserProfile(User user, Long id, String username, String profilePicture) {
		super();
		this.user = user;
		this.id = id;
		this.username = username;
		this.profilePicture = profilePicture;
	}
	
	@Override
	public String toString() {
		return "Profile [user=" + user + ", id=" + id + ", username=" + username + ", profilePicture=" + profilePicture
				+ "]";
	}

	
	
}
