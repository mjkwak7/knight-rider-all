package edu.mga.knightrider.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

/**
 * UserRole
 * 
 */
@Entity
@Table(name = "user_role")
public class UserRole {
    @Embeddable
    public static class Id implements Serializable {
        private static final long serialVersionUID = 1322120000551624359L;
        
        @Column(name = "user_id")
        protected Long userId;
        
        @Enumerated(EnumType.STRING)
        @Column(name = "role")
        protected Role role;
        
        public Id() { }

        public Id(Long userId, Role role) {
            this.userId = userId;
            this.role = role;
        }
    }
    
    @EmbeddedId
    Id id = new Id();
    
    public void setId(Id id) {
		this.id = id;
	}

	@Enumerated(EnumType.STRING)
    @Column(name = "ROLE", insertable=false, updatable=false)
    protected Role role;

    public Role getRole() {
        return role;
    }
    
	@Column(name = "user_id", insertable=false, updatable=false)
    protected Long userId;
    
	public Long getUserId() {
		return userId;
	}

	@Override
	public String toString() {
		return "UserRole [id=" + id + ", role=" + role + ", userId=" + userId + "]";
	}
	
	
}
