package edu.mga.knightrider.model.viewmodel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import edu.mga.knightrider.model.Passenger;

public class PassengerListDeserializer extends StdDeserializer<List<Passenger>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PassengerListDeserializer() {
        this(null);
    }
 
    public PassengerListDeserializer(Class<?> vc) {
        super(vc);
    }

	@Override
	public List<Passenger> deserialize(JsonParser arg0, DeserializationContext arg1)
			throws IOException, JsonProcessingException {
		return new ArrayList<>();
	}
}
