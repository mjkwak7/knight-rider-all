package edu.mga.knightrider.model.viewmodel;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class PassengerViewModel {

	public PassengerViewModel() {
		// TODO Auto-generated constructor stub
	}
	
	public PassengerViewModel(Long userId, Long tripId, Date joinDate, String firstName, String lastName, String profilePicture) {
		super();
		this.userId = userId;
		this.tripId = tripId;
		this.joinDate = joinDate;
		this.firstName = firstName;
		this.lastName = lastName;
		this.profilePicture = profilePicture;
	}


	@NotNull
	@Min(value = 1, message="User ID is missing!")
	private Long userId;
	
	@NotNull
	@Min(value = 1, message="Trip ID is missing!")
	private Long tripId;
	
	private Date joinDate;
	
	private String firstName;

	private String lastName;
	
	private String profilePicture;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getTripId() {
		return tripId;
	}
	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	@Override
	public String toString() {
		return "PassengerViewModel [userId=" + userId + ", tripId=" + tripId + ", joinDate=" + joinDate + ", firstName="
				+ firstName + ", lastName=" + lastName + ", profilePicture=" + profilePicture + "]";
	}

	
		

}
