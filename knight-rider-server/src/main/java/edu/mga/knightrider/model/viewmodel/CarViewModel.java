package edu.mga.knightrider.model.viewmodel;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class CarViewModel {

	
	private Long id;
	
	@NotBlank(message="Car maker is missing!")
	@NotNull
    @Size(min = 1)
	private String maker;
	
	@NotBlank(message="Car type is missing!")
	@NotNull
    @Size(min = 1)
	private String type;
	
	@NotNull
    @Min(value = 1)
	private Integer capacity;
	
	public CarViewModel() {
		super();
	}
	public CarViewModel(Long id, String maker, String type, Integer capacity) {
		super();		
		this.id = id;
		this.maker = maker;
		this.type = type;
		this.capacity = capacity;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMaker() {
		return maker;
	}
	public void setMaker(String maker) {
		this.maker = maker;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getCapacity() {
		return capacity;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	@Override
	public String toString() {
		return "CarViewModel [id=" + id + ", maker=" + maker + ", type=" + type + ", capacity=" + capacity + "]";
	}
	
	
	
}
