package edu.mga.knightrider.model;

/**
 * Enumerated {@link User} roles.
 * 
 * 
 */
public enum Role {
    ADMIN, PREMIUM_MEMBER, MEMBER, FACULTY, STAFF, STUDENT;
    
    public String authority() {
        return "ROLE_" + this.name();
    }
}
