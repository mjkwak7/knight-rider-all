package edu.mga.knightrider.model.viewmodel;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.Future;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import edu.mga.knightrider.model.Car;



public class TripViewModel {

	private Long id;
	
	@NotNull(message="Car information is missing!")
	@Min(value = 1)
	private Long carId;
	
	@NotNull(message="Driver information is missing!")
    @Min(value = 1)
	private Long driverId;
	
	@NotBlank(message="Origin address is missing!")
	@NotNull(message="Origin address is missing!")
    @Size(min = 1)
	private String originAddress;
	
	private String originCity;
	private String originState;
	private String originZip;
	private Double originLatitude;
	private Double originLongitude;
	
	@NotBlank(message="Destination information is missing!")
	@NotNull(message="Destination information is missing!")
    @Size(min = 1)
	private String destAddress;
	private String destCity;
	private String destState;
	private String destZip;
	private Double destLatitude;
	private Double destLongitude;
	
	@NotNull (message="Departure datetime is missing.")
	@Future (message="Only future date is possible.")
	private Date departureTime;
	
	@NotBlank(message="Meeting location is missing!")
	@NotNull(message="Meeting location is missing!")
    @Size(min = 1)
	private String meetingLocation;
	private Double meetingLatitude;
	private Double meetingLongitude;
	
	@NotNull(message="The number of available seats is missing!")
	@Min(value = 1)
	private Integer availableSeats;
	private Integer remainingSeats;
	
	private CarViewModel car;
	private DriverViewModel driver;
	private List<UserViewModel> passengers;
	private List<MessageViewModel> messages;
	
	public TripViewModel() {
		super();
	}

	public TripViewModel(Long id, Long carId, Long driverId, String originAddress, String originCity,
			String originState, String originZip, Double originLatitude, Double originLongitude, String destAddress,
			String destCity, String destState, String destZip, Double destLatitude, Double destLongitude,
			Date departureTime, String meetingLocation, Double meetingLatitude, Double meetingLongitude,
			Integer availableSeats, Integer remainingSeats, CarViewModel car, DriverViewModel driver,
			List<UserViewModel> passengers,List<MessageViewModel> messages) {
		super();		
		this.id = id;
		this.carId = carId;
		this.driverId = driverId;
		this.originAddress = originAddress;
		this.originCity = originCity;
		this.originState = originState;
		this.originZip = originZip;
		this.originLatitude = originLatitude;
		this.originLongitude = originLongitude;
		this.destAddress = destAddress;
		this.destCity = destCity;
		this.destState = destState;
		this.destZip = destZip;
		this.destLatitude = destLatitude;
		this.destLongitude = destLongitude;
		this.departureTime = departureTime;
		this.meetingLocation = meetingLocation;
		this.meetingLatitude = meetingLatitude;
		this.meetingLongitude = meetingLongitude;
		this.availableSeats = availableSeats;
		this.remainingSeats = remainingSeats;
		this.car = car;
		this.driver = driver;
		this.passengers = passengers;
		this.messages = messages;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}

	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	public String getOriginAddress() {
		return originAddress;
	}

	public void setOriginAddress(String originAddress) {
		this.originAddress = originAddress;
	}

	public String getOriginCity() {
		return originCity;
	}

	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	public String getOriginState() {
		return originState;
	}

	public void setOriginState(String originState) {
		this.originState = originState;
	}

	public String getOriginZip() {
		return originZip;
	}

	public void setOriginZip(String originZip) {
		this.originZip = originZip;
	}

	public Double getOriginLatitude() {
		return originLatitude;
	}

	public void setOriginLatitude(Double originLatitude) {
		this.originLatitude = originLatitude;
	}

	public Double getOriginLongitude() {
		return originLongitude;
	}

	public void setOriginLongitude(Double originLongitude) {
		this.originLongitude = originLongitude;
	}

	public String getDestAddress() {
		return destAddress;
	}

	public void setDestAddress(String destAddress) {
		this.destAddress = destAddress;
	}

	public String getDestCity() {
		return destCity;
	}

	public void setDestCity(String destCity) {
		this.destCity = destCity;
	}

	public String getDestState() {
		return destState;
	}

	public void setDestState(String destState) {
		this.destState = destState;
	}

	public String getDestZip() {
		return destZip;
	}

	public void setDestZip(String destZip) {
		this.destZip = destZip;
	}

	public Double getDestLatitude() {
		return destLatitude;
	}

	public void setDestLatitude(Double destLatitude) {
		this.destLatitude = destLatitude;
	}

	public Double getDestLongitude() {
		return destLongitude;
	}

	public void setDestLongitude(Double destLongitude) {
		this.destLongitude = destLongitude;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public String getMeetingLocation() {
		return meetingLocation;
	}

	public void setMeetingLocation(String meetingLocation) {
		this.meetingLocation = meetingLocation;
	}

	public Double getMeetingLatitude() {
		return meetingLatitude;
	}

	public void setMeetingLatitude(Double meetingLatitude) {
		this.meetingLatitude = meetingLatitude;
	}

	public Double getMeetingLongitude() {
		return meetingLongitude;
	}

	public void setMeetingLongitude(Double meetingLongitude) {
		this.meetingLongitude = meetingLongitude;
	}

	public Integer getAvailableSeats() {
		return availableSeats;
	}

	public void setAvailableSeats(Integer availableSeats) {
		this.availableSeats = availableSeats;
	}

	public Integer getRemainingSeats() {
		return remainingSeats;
	}

	public void setRemainingSeats(Integer remainingSeats) {
		this.remainingSeats = remainingSeats;
	}
	
	public CarViewModel getCar() {
		return car;
	}

	public void setCar(CarViewModel car) {
		this.car = car;
	}

	public DriverViewModel getDriver() {
		return driver;
	}

	public void setDriver(DriverViewModel driver) {
		this.driver = driver;
	}	

	public List<UserViewModel> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<UserViewModel> passengers) {
		this.passengers = passengers;
	}

	public List<MessageViewModel> getMessages() {
		return messages;
	}

	public void setMessages(List<MessageViewModel> messages) {
		this.messages = messages;
	}

	@Override
	public String toString() {
		return "TripViewModel [id=" + id + ", carId=" + carId + ", driverId=" + driverId + ", originAddress="
				+ originAddress + ", originCity=" + originCity + ", originState=" + originState + ", originZip="
				+ originZip + ", originLatitude=" + originLatitude + ", originLongitude=" + originLongitude
				+ ", destAddress=" + destAddress + ", destCity=" + destCity + ", destState=" + destState + ", destZip="
				+ destZip + ", destLatitude=" + destLatitude + ", destLongitude=" + destLongitude + ", departureTime="
				+ departureTime + ", meetingLocation=" + meetingLocation + ", meetingLatitude=" + meetingLatitude
				+ ", meetingLongitude=" + meetingLongitude + ", availableSeats=" + availableSeats + ", remainingSeats="
				+ remainingSeats + ", car=" + car + ", driver=" + driver + ", passengers=" + passengers + ", messages="
				+ messages + "]";
	}


}
