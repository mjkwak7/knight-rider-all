package edu.mga.knightrider.model.viewmodel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import edu.mga.knightrider.model.Passenger;

public class PassengerListSerializer extends StdSerializer<List<Passenger>> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PassengerListSerializer()
	{
		this(null);
	}

	public PassengerListSerializer(Class<List<Passenger>> t) {
        super(t);
    }
	
	@Override
	public void serialize(List<Passenger> list, JsonGenerator generator, SerializerProvider provider) throws IOException, JsonProcessingException {
		
	         
	        List<PassengerViewModel> passengers = new ArrayList<PassengerViewModel>();
	        for (Passenger p : list) {
	            //passengers.add(new PassengerViewModel(p.getUser().getId(), p.getTrip().getId(), p.getJoinDate()));
	        }
	        generator.writeObject(passengers);
		
	}
}
