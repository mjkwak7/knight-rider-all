package edu.mga.knightrider.model;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="app_user")
public class User {

	@OneToMany
	@JoinColumn(name = "user_id", referencedColumnName="ID", insertable=false, updatable=false)
	private Set<Car> cars;

	
	public Set<Car> getCars() {
		return cars;
	}
	
	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName="ID", insertable=false, updatable=false)
	private UserProfile profile;
		
	public UserProfile getProfile() {
		return profile;
	}

	@OneToMany
	@JoinColumn(name = "user_id", referencedColumnName="ID", insertable=false, updatable=false)
	private Set<Trip> trips;
	
	public Set<Trip> getTrips() {
		return trips;
	}

	@JsonIgnore
    @OneToMany
    @JoinColumn(name="user_id", referencedColumnName="ID", insertable=false, updatable=false)
    private List<UserRole> roles;


	@Id @Column(name="ID")
	@GeneratedValue
	private Long id;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	
    @Column(name="username")
    private String username;
    
    @Column(name="password")
    private String password;

    @Column(name="firstname")
	private String firstName;
    
    @Column(name="lastname")
	private String lastName;
    
    @Column(name="address")
	private String address;
    
    @Column(name="zip")
	private String zip;
    
    @Column(name="phone")
	private String phone;

    @Column(name="enabled")
    private boolean enabled;    
    
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}	


	
	public User() {
		super();
		this.enabled = false;
	}
		
	
    public User(String username, String password, String firstName, String lastName, String address, String zip,
			String phone) {
		super();
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.zip = zip;
		this.phone = phone;
		this.enabled = false;
	}
    
	public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
		this.password = password;
	}
	public List<UserRole> getRoles() {
        return roles;
    }
	public void setRoles(List<UserRole> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", address=" + address + ", zip=" + zip + ", phone=" + phone + ", enabled=" + enabled +"]";
	}
	
	
}
