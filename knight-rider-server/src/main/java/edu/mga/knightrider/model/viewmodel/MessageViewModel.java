package edu.mga.knightrider.model.viewmodel;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class MessageViewModel {

	private Long id;
	private Date logDate;

	@NotBlank(message = "Comment is missing!")
	@NotNull
	@Size(min = 1)
	private String comment;

	@NotNull
	@Min(value = 1, message = "Trip ID is missing!")
	private Long tripId;

	@NotNull
	@Min(value = 1, message = "User ID is missing!")
	private Long userId;

	private String firstName;

	private String lastName;
	
	private String profilePicture;

	private TripViewModel trip;
	private UserViewModel user;

	public MessageViewModel() {
		super();
	}

	public MessageViewModel(Long id, Date logDate, String comment, Long tripId, Long userId, String firstName,
			String lastName, String profilePicture) {
		super();
		this.id = id;
		this.logDate = logDate;
		this.comment = comment;
		this.tripId = tripId;
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.profilePicture = profilePicture;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getTripId() {
		return tripId;
	}

	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public TripViewModel getTrip() {
		return trip;
	}

	public void setTrip(TripViewModel trip) {
		this.trip = trip;
	}

	public UserViewModel getUser() {
		return user;
	}

	public void setUser(UserViewModel user) {
		this.user = user;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	@Override
	public String toString() {
		return "MessageViewModel [id=" + id + ", logDate=" + logDate + ", comment=" + comment + ", tripId=" + tripId
				+ ", userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", trip=" + trip
				+ ", user=" + user + "]";
	}

}
