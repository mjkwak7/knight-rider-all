package edu.mga.knightrider.model.viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mga.knightrider.model.Car;
import edu.mga.knightrider.model.Message;
import edu.mga.knightrider.model.Passenger;
import edu.mga.knightrider.model.Review;
import edu.mga.knightrider.model.Trip;
import edu.mga.knightrider.model.User;
import edu.mga.knightrider.model.UserProfile;
import edu.mga.knightrider.service.CarService;
import edu.mga.knightrider.service.TripService;
import edu.mga.knightrider.service.UserProfileService;
import edu.mga.knightrider.service.UserService;

@Service
public class ViewModelHelper {

	private static Logger logger = LoggerFactory.getLogger(ViewModelHelper.class);

	@Autowired
	UserService userService;

	@Autowired
	TripService tripService;

	@Autowired
	CarService carService;
	
	@Autowired
	UserProfileService userProfileService;

	public UserViewModel getUserViewModelFromUser(User u) {

		UserViewModel user = new UserViewModel();
		user.setId(u.getId());
		user.setUsername(u.getUsername());
		user.setFirstName(u.getFirstName());
		user.setLastName(u.getLastName());
		user.setAddress(u.getAddress());
		user.setZip(u.getZip());
		user.setPhone(u.getPhone());
		
			
		UserProfile profile = userProfileService.findOneByUserId(user.getId());		
		if (profile != null){
			user.setProfilePicture(profile.getProfilePicture());
		}
		

		// user.setTrips(u.getTrips());
		List<TripViewModel> trips = new ArrayList<TripViewModel>();
		for (Trip t : u.getTrips()) {
			TripViewModel trip = getTripViewModelFromTripAndUser(t, u);

			trip.setMessages(null);
			trip.setPassengers(null);

			trips.add(trip);
		}
		user.setTrips(trips);

		List<CarViewModel> cars = new ArrayList<CarViewModel>();
		for (Car c : u.getCars()) {
			CarViewModel car = new CarViewModel(c.getId(), c.getMaker(), c.getType(), c.getCapacity());
			cars.add(car);
		}
		user.setCars(cars);

		return user;
	}

	/*
	 * public TripViewModel getTripViewModelFromTripAndUser(Trip trip, User
	 * user){
	 * 
	 * DriverViewModel driver = new DriverViewModel(user.getId(),
	 * user.getUsername(), user.getFirstName(), user.getLastName(),
	 * user.getAddress(), user.getZip(), user.getPhone());
	 * 
	 * 
	 * TripViewModel tripViewModel = new TripViewModel( trip.getId(),
	 * trip.getCar().getId(), driver.getDriverId(), trip.getOriginAddress(),
	 * trip.getOriginCity(), trip.getOriginState(),trip.getOriginZip(),
	 * trip.getOriginLatitude(), trip.getOriginLongitude(),
	 * trip.getDestAddress(), trip.getDestCity(), trip.getDestState(),
	 * trip.getDestZip(), trip.getDestLatitude(), trip.getDestLongitude(),
	 * trip.getDepartureTime(), trip.getMeetingLocation(),
	 * trip.getMeetingLatitude(), trip.getMeetingLongitude(),
	 * trip.getAvailableSeats(), trip.getRemainingSeats(), trip.getCar(),
	 * driver, null, null);
	 * 
	 * return tripViewModel; }
	 * 
	 */

	public CarViewModel getCarViewModel(Car car){
		
		return new CarViewModel(car.getId(), car.getMaker(), car.getType(), car.getCapacity());
	}
	
	public TripViewModel getTripViewModel(Long tripId) {
		Trip trip = tripService.getTrip(tripId);
		return getTripViewModelFromTrip(trip);
	}

	public TripViewModel getTripViewModelFromTrip(Trip trip) {

		logger.info("trip : " + trip);

		User u = userService.getUser(trip.getUser().getId());

		List<Passenger> temp = trip.getPassengers();
		List<UserViewModel> passengers = null;
		if (temp != null) {
			passengers = new ArrayList<UserViewModel>();
			for (Passenger p : temp) {
				passengers.add(getUserViewModelFromPassenger(p));
			}
		}

		List<Message> temp1 = trip.getMessages();
		List<MessageViewModel> messages = null;
		if (temp1 != null) {
			messages = new ArrayList<MessageViewModel>();
			for (Message m : temp1) {
				messages.add(getMessageViewModelFromMessage(m));
			}
		}
		
		UserProfile profile = userProfileService.findOneByUserId(u.getId());
		String profilePicture = "";
		if (profile != null)
			profilePicture = profile.getProfilePicture();

		DriverViewModel driver = new DriverViewModel(u.getId(), u.getUsername(), u.getFirstName(), u.getLastName(),
				u.getAddress(), u.getZip(), u.getPhone(), profilePicture);
		
		CarViewModel car = new CarViewModel();
		Long carId = null;
		if ( trip.getCar() != null){
			carId =  trip.getCar().getId();
			car = new CarViewModel( trip.getCar().getId(),  trip.getCar().getMaker(),  trip.getCar().getType(),  trip.getCar().getCapacity());
		}

		TripViewModel tripViewModel = new TripViewModel(trip.getId(), carId, driver.getDriverId(),
				trip.getOriginAddress(), trip.getOriginCity(), trip.getOriginState(), trip.getOriginState(),
				trip.getOriginLatitude(), trip.getOriginLongitude(), trip.getDestAddress(), trip.getDestCity(),
				trip.getDestState(), trip.getDestZip(), trip.getDestLatitude(), trip.getDestLongitude(),
				trip.getDepartureTime(), trip.getMeetingLocation(), trip.getMeetingLatitude(),
				trip.getMeetingLongitude(), trip.getAvailableSeats(), trip.getRemainingSeats(), car, driver,
				passengers, messages);
		return tripViewModel;
	}

	public TripViewModel getTripViewModelFromTripAndUser(Trip trip, User user) {

		UserProfile profile = userProfileService.findOneByUserId(user.getId());
		String profilePicture = "";
		if (profile != null)
			profilePicture = profile.getProfilePicture();
		
		DriverViewModel driver = new DriverViewModel(user.getId(), user.getUsername(), user.getFirstName(),
				user.getLastName(), user.getAddress(), user.getZip(), user.getPhone(), profilePicture);

		List<Passenger> temp = trip.getPassengers();
		List<UserViewModel> passengers = null;
		if (temp != null) {
			passengers = new ArrayList<UserViewModel>();
			for (Passenger p : temp) {
				passengers.add(getUserViewModelFromPassenger(p));
			}
		}

		List<Message> temp1 = trip.getMessages();
		List<MessageViewModel> messages = null;
		if (temp1 != null) {
			messages = new ArrayList<MessageViewModel>();
			for (Message m : temp1) {
				messages.add(getMessageViewModelFromMessage(m));
			}
		}
		
		Long carId = null;
		CarViewModel car = new CarViewModel();
		if ( trip.getCar() != null){
			carId =  trip.getCar().getId();
			car = new CarViewModel( trip.getCar().getId(),  trip.getCar().getMaker(),  trip.getCar().getType(),  trip.getCar().getCapacity());
		}

		TripViewModel tripViewModel = new TripViewModel(trip.getId(), carId, driver.getDriverId(),
				trip.getOriginAddress(), trip.getOriginCity(), trip.getOriginState(), trip.getOriginZip(),
				trip.getOriginLatitude(), trip.getOriginLongitude(), trip.getDestAddress(), trip.getDestCity(),
				trip.getDestState(), trip.getDestZip(), trip.getDestLatitude(), trip.getDestLongitude(),
				trip.getDepartureTime(), trip.getMeetingLocation(), trip.getMeetingLatitude(),
				trip.getMeetingLongitude(), trip.getAvailableSeats(), trip.getRemainingSeats(), car, driver,
				passengers, messages);
		return tripViewModel;
	}

	public Trip getTripFromUserIdAndTripViewModel(Long userId, TripViewModel trip) {
		User user = userService.getUser(userId);
		Car car = carService.getCar(trip.getCarId());

		Trip t = new Trip(user, car, trip.getId(), trip.getOriginAddress(), trip.getOriginCity(), trip.getOriginState(),
				trip.getOriginZip(), trip.getOriginLatitude(), trip.getOriginLongitude(), trip.getDestAddress(),
				trip.getDestCity(), trip.getDestState(), trip.getDestZip(), trip.getDestLatitude(),
				trip.getDestLongitude(), trip.getDepartureTime(), trip.getMeetingLocation(), trip.getMeetingLatitude(),
				trip.getMeetingLongitude(), trip.getAvailableSeats(), trip.getRemainingSeats());

		return t;
	}

	public UserViewModel getUserViewModelFromPassenger(Passenger passenger) {

		if (passenger != null) {
			User user = userService.getUser(passenger.getUserId());
			UserProfile profile = userProfileService.findOneByUserId(user.getId());
			String profilePicture = "";
			if (profile != null)
				profilePicture = profile.getProfilePicture();
			
			return new UserViewModel(user.getId(), user.getUsername(), user.getFirstName(), user.getLastName(),
					user.getAddress(), user.getZip(), user.getPhone(), profilePicture);
		} else {
			return null;
		}
	}

	public MessageViewModel getMessageViewModelFromMessage(Message message) {
		if (message != null) {
			User user = userService.getUser(message.getUserId());
			UserProfile profile = userProfileService.findOneByUserId(user.getId());
			String profilePicture = "";
			if (profile != null)
				profilePicture = profile.getProfilePicture();
			
			return new MessageViewModel(message.getId(), message.getLogDate(), message.getComment(),
					message.getTripId(), message.getUserId(), user.getFirstName(), user.getLastName(), profilePicture);
		} else {
			return null;
		}
	}

	public ReviewViewModel getReviewViewModelFromReview(Review review) {
		if (review != null) {
			
			User user = userService.getUser(review.getUserId());
			
			UserProfile profile = userProfileService.findOneByUserId(user.getId());
			String profilePicture = "";
			if (profile != null)
				profilePicture = profile.getProfilePicture();
			
			return new ReviewViewModel(review.getId(), review.getLogDate(), review.getComment(), review.getScore(),
					review.getTripId(), review.getUserId(), user.getFirstName(), user.getLastName(), profilePicture);
		} else {
			return null;
		}
	}
	
	public PassengerViewModel getPassengerViewModelFromPassenger(Passenger passenger) {
		if (passenger != null && passenger.getUserId() != null) {
			
			User user = userService.getUser(passenger.getUserId());
			UserProfile profile = userProfileService.findOneByUserId(user.getId());
			String profilePicture = "";
			if (profile != null)
				profilePicture = profile.getProfilePicture();
			
			return new PassengerViewModel(passenger.getUserId(), passenger.getTripId(), passenger.getJoinDate(), 
					user.getFirstName(), user.getLastName(), profilePicture);
			
		} else {
			return null;
		}
	}

}
