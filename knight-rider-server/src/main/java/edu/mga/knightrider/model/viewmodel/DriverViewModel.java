package edu.mga.knightrider.model.viewmodel;

import java.util.Set;


public class DriverViewModel {

	private Long driverId;	
    private String username;    
	private String firstName;    
	private String lastName;    
	private String address;    
	private String zip;    
	private String phone;
	private String profilePicture;
	
	public DriverViewModel() {
		super();
	}

	public DriverViewModel(Long driverId, String username, String firstName, String lastName, String address,
			String zip, String phone, String profilePicture) {
		super();
		this.driverId = driverId;
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.zip = zip;
		this.phone = phone;
		this.profilePicture = profilePicture;
	}

	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	
	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	@Override
	public String toString() {
		return "DriverViewModel [driverId=" + driverId + ", username=" + username + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", address=" + address + ", zip=" + zip + ", phone=" + phone
				+ ", profilePicture=" + profilePicture + "]";
	}
	
	
}
