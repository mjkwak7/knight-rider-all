package edu.mga.knightrider.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="review")
public class Review {

	@Id 
	@Column(name="id")
	@GeneratedValue
	private Long id;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
    
	
	@Column(name="log_date")
	private Date logDate;
	
	@Column(name="comment")
	private String comment;
	
	@Column(name="score")
	private Integer score;
	
	public Date getLogDate() {
		return logDate;
	}
	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Column(name = "user_id")
    protected Long userId;

	@Column(name = "trip_id")
    protected Long tripId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getTripId() {
		return tripId;
	}

	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}

	public Review() {
		super();
	}

	public Review(Long id, Date logDate, String comment, Integer score, Long userId, Long tripId) {
		super();
		this.id = id;
		this.logDate = logDate;
		this.comment = comment;
		this.score = score;
		this.userId = userId;
		this.tripId = tripId;
	}

	@Override
	public String toString() {
		return "Review [id=" + id + ", logDate=" + logDate + ", comment=" + comment + ", score=" + score + ", userId="
				+ userId + ", tripId=" + tripId + "]";
	}     
		
   
}
