package edu.mga.knightrider.model.viewmodel;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class UserViewModel {

	private Long id;
	
	@NotBlank(message="Username is missing!")
	@NotNull
    @Size(min = 1)
    private String username;   
	
	@NotBlank(message="Firstname is missing!")
	@NotNull
    @Size(min = 1)
	private String firstName; 
	
	@NotBlank(message="Lastname is missing!")
	@NotNull
    @Size(min = 1)
	private String lastName;  
	
	private String address;    
	private String zip;    
	private String phone;
	private String profilePicture;
	private List<CarViewModel> cars;
	private List<TripViewModel> trips;
	
	public UserViewModel(){		
		
	}
	
	public UserViewModel(Long id, String username, String firstName, String lastName, String address, String zip, String phone, String profilePicture){
		this.id = id;
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.zip = zip;
		this.phone = phone;
		this.profilePicture = profilePicture;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public List<CarViewModel> getCars() {
		return cars;
	}
	public void setCars(List<CarViewModel> cars) {
		this.cars = cars;
	}
	public List<TripViewModel> getTrips() {
		return trips;
	}
	public void setTrips(List<TripViewModel> trips) {
		this.trips = trips;
	}	
	
	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	@Override
	public String toString() {
		return "UserViewModel [id=" + id + ", username=" + username + ", firstName=" + firstName + ", lastName="
				+ lastName + ", address=" + address + ", zip=" + zip + ", phone=" + phone + ", cars=" + cars
				+ ", trips=" + trips + "]";
	}
 
	
}
