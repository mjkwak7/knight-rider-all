package edu.mga.knightrider.model.viewmodel;

public class TripSearchViewModel {

	String origin;
	String dest;
	
	public TripSearchViewModel() {
		super();
	}

	public TripSearchViewModel(String origin, String dest) {
		super();
		this.origin = origin;
		this.dest = dest;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	@Override
	public String toString() {
		return "TripSearchViewModel [origin=" + origin + ", dest=" + dest + "]";
	}
	
	
}
