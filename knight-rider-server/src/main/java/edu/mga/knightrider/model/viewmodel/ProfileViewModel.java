package edu.mga.knightrider.model.viewmodel;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class ProfileViewModel {
	
	private long id;
	
	@NotBlank(message="Base64 encoded image is missing!")
	@NotNull
	private String profilePicture;
	
	@NotBlank(message="File extension is missing!")
	@NotNull
	private String extension;
	
	public ProfileViewModel() {
		super();
	}
		
	public ProfileViewModel(String profilePicture, String extension) {
		super();
		this.profilePicture = profilePicture;
		this.extension = extension;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getProfilePicture() {
		return profilePicture;
	}
	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	

}
