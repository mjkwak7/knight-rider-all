package edu.mga.knightrider.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="trip")
public class Trip {

	//@JsonIgnore
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="ID")
	private User user;
	
	public User getUser(){
		return this.user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	//@Column(name = "user_id", insertable=false, updatable=false)
    //protected Long userId;
	
    //public Long getUserId() {
	//	return userId;
	//} 
	
	@OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true)
	@JoinColumn(name="trip_id", referencedColumnName="ID", insertable=false, updatable=false)
	private List<Passenger> passengers;

	public List<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}

	@OneToMany (cascade = CascadeType.REMOVE, orphanRemoval = true)
	@JoinColumn(name="trip_id", referencedColumnName="ID", insertable=false, updatable=false)
	private List<Message> messages;
	
	public List<Message> getMessages() {
		return messages;
	}
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	
	@ManyToOne 
	@JoinColumn(name="car_id", referencedColumnName="ID")
	private Car car;	
	
	public Car getCar() {
		return car;
	}
	public void setCar(Car car) {
		this.car = car;
	}

	@Id @Column(name="id")
	@GeneratedValue
	private Long id;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="origin_address")
	private String originAddress;
	
	@Column(name="origin_city")
	private String originCity;
	
	@Column(name="origin_state")
	private String originState;
	
	@Column(name="origin_zip")
	private String originZip;
	
	@Column(name="origin_latitude")
	private Double originLatitude;

	@Column(name="origin_longitude")
	private Double originLongitude;
	
	@Column(name="dest_address")
	private String destAddress;
	
	@Column(name="dest_city")
	private String destCity;
	
	@Column(name="dest_state")
	private String destState;
	
	@Column(name="dest_zip")
	private String destZip;

	@Column(name="dest_latitude")
	private Double destLatitude;

	@Column(name="dest_longitude")
	private Double destLongitude;
	
	@Column(name="dept_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date departureTime;
	
	@Column(name="meet_location")
	private String meetingLocation;
	
	@Column(name="meet_latitude")
	private Double meetingLatitude;

	@Column(name="meet_longitude")
	private Double meetingLongitude;

	@Column(name="available_seats")
	private Integer availableSeats;

	@Column(name="remaining_seats")
	private Integer remainingSeats;
	
	
	public Trip() {}	

	public Trip(User user, Car car, Long id, String originAddress, String originCity, String originState,
			String originZip, Double originLatitude, Double originLongitude, String destAddress, String destCity,
			String destState, String destZip, Double destLatitude, Double destLongitude, Date departureTime,
			String meetingLocation, Double meetingLatitude, Double meetingLongitude, Integer availableSeats,
			Integer remainingSeats) {
		super();
		this.user = user;
		this.car = car;
		this.id = id;
		this.originAddress = originAddress;
		this.originCity = originCity;
		this.originState = originState;
		this.originZip = originZip;
		this.originLatitude = originLatitude;
		this.originLongitude = originLongitude;
		this.destAddress = destAddress;
		this.destCity = destCity;
		this.destState = destState;
		this.destZip = destZip;
		this.destLatitude = destLatitude;
		this.destLongitude = destLongitude;
		this.departureTime = departureTime;
		this.meetingLocation = meetingLocation;
		this.meetingLatitude = meetingLatitude;
		this.meetingLongitude = meetingLongitude;
		this.availableSeats = availableSeats;
		this.remainingSeats = remainingSeats;
	}
	
	
	
	public String getOriginAddress() {
		return originAddress;
	}
	public void setOriginAddress(String originAddress) {
		this.originAddress = originAddress;
	}
	public String getOriginCity() {
		return originCity;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	public String getOriginState() {
		return originState;
	}
	public void setOriginState(String originState) {
		this.originState = originState;
	}
	public String getOriginZip() {
		return originZip;
	}
	public void setOriginZip(String originZip) {
		this.originZip = originZip;
	}
	public Double getOriginLatitude() {
		return originLatitude;
	}
	public void setOriginLatitude(Double originLatitude) {
		this.originLatitude = originLatitude;
	}
	public Double getOriginLongitude() {
		return originLongitude;
	}
	public void setOriginLongitude(Double originLongitude) {
		this.originLongitude = originLongitude;
	}
	public String getDestAddress() {
		return destAddress;
	}
	public void setDestAddress(String destAddress) {
		this.destAddress = destAddress;
	}
	public String getDestCity() {
		return destCity;
	}
	public void setDestCity(String destCity) {
		this.destCity = destCity;
	}
	public String getDestState() {
		return destState;
	}
	public void setDestState(String destState) {
		this.destState = destState;
	}
	public String getDestZip() {
		return destZip;
	}
	public void setDestZip(String destZip) {
		this.destZip = destZip;
	}
	public Double getDestLatitude() {
		return destLatitude;
	}
	public void setDestLatitude(Double destLatitude) {
		this.destLatitude = destLatitude;
	}
	public Double getDestLongitude() {
		return destLongitude;
	}
	public void setDestLongitude(Double destLongitude) {
		this.destLongitude = destLongitude;
	}
	public Date getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}
	public String getMeetingLocation() {
		return meetingLocation;
	}
	public void setMeetingLocation(String meetingLocation) {
		this.meetingLocation = meetingLocation;
	}
	public Double getMeetingLatitude() {
		return meetingLatitude;
	}
	public void setMeetingLatitude(Double meetingLatitude) {
		this.meetingLatitude = meetingLatitude;
	}
	public Double getMeetingLongitude() {
		return meetingLongitude;
	}
	public void setMeetingLongitude(Double meetingLongitude) {
		this.meetingLongitude = meetingLongitude;
	}
	public Integer getAvailableSeats() {
		return availableSeats;
	}
	public void setAvailableSeats(Integer availableSeats) {
		this.availableSeats = availableSeats;
	}
	public Integer getRemainingSeats() {
		return remainingSeats;
	}
	public void setRemainingSeats(Integer remainingSeats) {
		this.remainingSeats = remainingSeats;
	}
	
	@Override
	public String toString() {
		return "Trip [user=" + user + ", passengers=" + passengers + ", messages=" + messages + ", id="
				+ id + ", originAddress=" + originAddress + ", originCity=" + originCity + ", originState="
				+ originState + ", originZip=" + originZip + ", originLatitude=" + originLatitude + ", originLongitude="
				+ originLongitude + ", destAddress=" + destAddress + ", destCity=" + destCity + ", destState="
				+ destState + ", destZip=" + destZip + ", destLatitude=" + destLatitude + ", destLongitude="
				+ destLongitude + ", departureTime=" + departureTime + ", meetingLocation=" + meetingLocation
				+ ", meetingLatitude=" + meetingLatitude + ", meetingLongitude=" + meetingLongitude
				+ ", availableSeats=" + availableSeats + ", remainingSeats=" + remainingSeats + "]";
	}
	

}
