package edu.mga.knightrider.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="message")
public class Message {
	
	@Id 
	@Column(name="id")
	@GeneratedValue
	private Long id;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
    
	
	@Column(name="log_date")
	private Date logDate;
	
	@Column(name="comment")
	private String comment;
	
	public Date getLogDate() {
		return logDate;
	}
	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	@Column(name = "user_id")
    protected Long userId;

	@Column(name = "trip_id")
    protected Long tripId;     
   
    public Long getUserId() {
		return userId;
	}
    
	public Long getTripId() {
		return tripId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ",\n logDate=" + logDate + ",\n comment=" + comment + ",\n userId=" + userId
				+ ",\n tripId=" + tripId + "]";
	}

	
    

}
