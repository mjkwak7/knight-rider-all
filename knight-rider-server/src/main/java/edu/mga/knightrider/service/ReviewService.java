package edu.mga.knightrider.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mga.knightrider.model.Message;
import edu.mga.knightrider.model.Review;
import edu.mga.knightrider.repository.ReviewRepository;

@Service
public class ReviewService {

	private static Logger logger = LoggerFactory.getLogger(ReviewService.class);
	
	@Autowired
	private ReviewRepository reviewRepository;
	
	public List<Review> getAllReviews(){
		return reviewRepository.findAll();
	}
	
	
	public Review addReview(Long tripId, Long userId, String comment, Integer score) {
		
		logger.debug("Adding a new review: " + tripId + ", " + userId + ", " + comment + ", " + score);
		
		Review review = new Review();
		review.setTripId(tripId);
		review.setUserId(userId);
		review.setComment(comment);
		review.setScore(score);
		review.setLogDate(new Date());
		
		return reviewRepository.saveAndFlush(review);
	}
	
	public List<Review> getAllReviewsByTripId(Long tripId){		
		List<Review> reviews = reviewRepository.findReviewsByTripId(tripId);				
		return reviews;
	}
	
	public List<Review> getAllReviewsByTripIdAndUserId(Long tripId, Long userId){		
		List<Review> reviews = reviewRepository.findByReviewsIdAndUserId(tripId, userId);				
		return reviews;
	}
	
	public void deleteReviewsByTripIdAndUserId(Long tripId, Long userId){
		reviewRepository.deleteByTripIdAndUserId(tripId, userId);	
	}
	
	public void deleteByReviewId(Long id){
		reviewRepository.delete(id);
	}
	
	public void deleteByUserId(Long id){
		reviewRepository.deleteByUserId(id);
	}
	
	public void deleteByTripId(Long id){
		reviewRepository.deleteByTripId(id);
	}
}
