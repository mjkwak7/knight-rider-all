package edu.mga.knightrider.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mga.knightrider.model.Passenger;
import edu.mga.knightrider.model.Trip;
import edu.mga.knightrider.repository.PassengerRepository;
import edu.mga.knightrider.repository.TripRepository;
import edu.mga.knightrider.repository.UserRepository;

@Service
public class PassengerService {

	private static Logger logger = LoggerFactory.getLogger(PassengerService.class);
	
	@Autowired
	private PassengerRepository passengerRepository;
	
	@Autowired
	private TripRepository tripRepository;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TripService tripService;

	/*
	 * Add new passenger
	 */
	@Transactional
	public boolean addPassenger(Long tripId, Long userId) {
		
		try {
			Trip trip = tripRepository.findOne(tripId);

			if (trip.getRemainingSeats() > 0) {

				logger.debug("Adding a new Passenger: " + tripId + " " + userId);
				Passenger.Id pk = new Passenger.Id(tripId, userId);

				Passenger passenger = new Passenger();
				passenger.setId(pk);
				passenger.setJoinDate(new Date());

				passengerRepository.saveAndFlush(passenger);
				tripService.decreaseRemainingSeats(tripId);

				logger.info("Successfully added!");

				return true;

			} else {
				logger.debug("No remaining seats!! ");
				return false;
			}

		} catch (RuntimeException e) {
			logger.debug(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	
	public List<Passenger> getAllPassengersByTripId(Long tripId){
		Trip trip = tripRepository.findOne(tripId);		
		return trip.getPassengers();
	}
	
	public boolean exist(Long tripId, Long userId){
		if(passengerRepository.findByPrimaryKey(tripId, userId) != null)
			return true;
		else 
			return false;
	}
	
	public List<Passenger> getAllPassengersByUserId(Long userId){
		//Trip trip = tripRepository.findOne(userId);
		return passengerRepository.findByUserId(userId);
	}
	
	@Transactional
	public void deletePassenger(Long tripId, Long userId){
		passengerRepository.deleteByPrimaryKey(tripId, userId);
		
		tripService.increaseRemainingSeats(tripId);
	}

	@Transactional
	public void deletePassengersByUserId(Long userId){
		List<Passenger> passengers = getAllPassengersByUserId(userId);
		for (Passenger passenger : passengers){
			deletePassenger(passenger.getTripId(), userId);
		}
	}
	
	public Passenger findByPrimaryKey(Long tripId, Long userId){
		return passengerRepository.findByPrimaryKey(tripId, userId);
	}
}
