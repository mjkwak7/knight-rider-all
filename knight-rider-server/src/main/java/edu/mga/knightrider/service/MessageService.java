package edu.mga.knightrider.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mga.knightrider.model.Message;

import edu.mga.knightrider.model.Trip;
import edu.mga.knightrider.repository.MessageRepository;
import edu.mga.knightrider.repository.TripRepository;


@Service
public class MessageService {

	private static Logger logger = LoggerFactory.getLogger(MessageService.class);
	
	@Autowired
	private MessageRepository messageRepository;
	
	@Autowired
	private TripRepository tripRepository;

	/*
	 * Add new message
	 */
	public Message addMessage(Long tripId, Long userId, String comment) {
		
		logger.debug("Adding a new message: " + tripId + ", " + userId + ", " + comment);
		
		Message message = new Message();
		message.setTripId(tripId);
		message.setUserId(userId);
		message.setComment(comment);
		message.setLogDate(new Date());
		
		return messageRepository.saveAndFlush(message);		
	}
	
	public List<Message> getAllMessagesByTripId(Long tripId){		
		List<Message> messages = messageRepository.findByTripId(tripId);				
		return messages;
	}
	
	public List<Message> getAllMessagesByTripIdAndUserId(Long tripId, Long userId){		
		List<Message> messages = messageRepository.findByTripIdAndUserId(tripId, userId);				
		return messages;
	}
	
	public void deleteMessagesByTripIdAndUserId(Long tripId, Long userId){
		messageRepository.deleteByTripIdAndUserId(tripId, userId);	
	}
	
	public void deleteByMessageId(Long id){
		messageRepository.delete(id);
	}
	
	public void deleteByUserId(Long id){
		messageRepository.deleteByUserId(id);
	}
	
	public void deleteByTripId(Long id){
		messageRepository.deleteByTripId(id);
	}
}
