package edu.mga.knightrider.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

import edu.mga.knightrider.model.Message;
import edu.mga.knightrider.model.Passenger;
import edu.mga.knightrider.model.Trip;
import edu.mga.knightrider.model.User;
import edu.mga.knightrider.model.viewmodel.TripSearchViewModel;
import edu.mga.knightrider.repository.MessageRepository;
import edu.mga.knightrider.repository.PassengerRepository;
import edu.mga.knightrider.repository.TripRepository;
import edu.mga.knightrider.repository.UserRepository;
import edu.mga.knightrider.service.search.SearchCriteria;
import edu.mga.knightrider.service.search.TripSpecification;

@Service
public class TripService {
	
	private static Logger logger = LoggerFactory.getLogger(TripService.class);

	@Autowired
	private TripRepository tripRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PassengerRepository passengerRepository;
	
	@Autowired
	private PassengerService passengerService;
	
	@Autowired
	private MessageRepository messageRepository;
	
	public List<Trip> getAllTripsForDriver(Long userId) {	
		return tripRepository.findByUserId(userId);
	}
		
	
	public List<Trip> getAllTripsForDriverAndPassenger(Long userId) {	
		
		List<Passenger> passengers = passengerService.getAllPassengersByUserId(userId);
		
		List<Trip> trips = new ArrayList<Trip>();
		for (Passenger p : passengers){
			trips.add(tripRepository.getOne(p.getTripId()));
		}
		
		trips.addAll(tripRepository.findByUserId(userId));
		
		Collections.sort(trips, new Comparator<Trip>() {
			  public int compare(Trip o1, Trip o2) {
			      return o1.getDepartureTime().compareTo(o2.getDepartureTime());
			  }
		});
		
		
		return trips;
	}
	
	public List<Trip> getAllTrips() {	
		return tripRepository.findAll();
	}	
	
	public List<Trip> searchTrips(TripSearchViewModel model) {	
		
		List<Trip> trips = new ArrayList<Trip>();
		
		if (!model.getOrigin().isEmpty() && !model.getDest().isEmpty()){
			TripSpecification spec1 = new TripSpecification(
					new SearchCriteria("originAddress", ":", model.getOrigin()));
			TripSpecification spec2 = new TripSpecification(new SearchCriteria("originCity", ":", model.getOrigin()));
			TripSpecification spec3 = new TripSpecification(new SearchCriteria("destAddress", ":", model.getDest()));
			TripSpecification spec4 = new TripSpecification(new SearchCriteria("destCity", ":", model.getDest()));
			trips = tripRepository.findAll(Specifications.where(spec1).or(spec2).or(spec3).or(spec4));
		} else if (!model.getOrigin().isEmpty()){
			TripSpecification spec1 = new TripSpecification(
					new SearchCriteria("originAddress", ":", model.getOrigin()));
			TripSpecification spec2 = new TripSpecification(new SearchCriteria("originCity", ":", model.getOrigin()));
			trips = tripRepository.findAll(Specifications.where(spec1).or(spec2));
		} else if (!model.getDest().isEmpty()){
			TripSpecification spec3 = new TripSpecification(new SearchCriteria("destAddress", ":", model.getDest()));
			TripSpecification spec4 = new TripSpecification(new SearchCriteria("destCity", ":", model.getDest()));
			trips = tripRepository.findAll(Specifications.where(spec3).or(spec4));
		}
			    
		return trips;
	}
	
	
	public Trip getTrip(Long id) {		
		return tripRepository.findOne(id);
	}

	public Trip addTrip(Trip trip) {
		return tripRepository.save(trip);
	}
	
	public Trip addTrip(Long userId, Trip trip) {
		if (trip.getUser() == null){
				User driver = userRepository.findOne(userId);
				trip.setUser(driver);
		}
		return tripRepository.save(trip);
	}
	

	public List<Trip> getTripsByCarId(Long carId){
		return tripRepository.findByCarId(carId);
	}
	
	
	public Trip updateTrip(Long id, Trip trip) {		
		trip.setId(id);
		return tripRepository.save(trip);
	}

	public Trip updateTrip(Long id, Long userId, Trip trip) {	
		
		//trip.setId(id);
		//User driver = userRepository.findOne(userId);
		//trip.setUser(driver);
		
		return tripRepository.save(trip);
	}

	@Transactional
	public void deleteTripByUserId(Long id){
		List<Trip> trips = getAllTripsForDriver(id);
		for (Trip trip : trips){
			deleteTrip(trip.getId());
		}
	}
	
	@Transactional
	public void deleteTrip(Long id) {		
		
		logger.info("delete trip --- " + id);
		
		Trip trip = tripRepository.findOne(id);
		if (trip.getPassengers() != null){			
			for (Passenger p : trip.getPassengers()){
				passengerRepository.delete(p);
			}
		}
		
		if (trip.getMessages()!=null){
			for (Message m : trip.getMessages()){
				messageRepository.delete(m);
			}
		}
		
		tripRepository.delete(id);	
	}
	
	public int getRemainingSeats(Long id){
		int i = tripRepository.findOne(id).getRemainingSeats();
		return i;
	}
	
	public void decreaseRemainingSeats(Long id){
		Trip trip = tripRepository.findOne(id);
		trip.setRemainingSeats(trip.getRemainingSeats() - 1);
		tripRepository.save(trip);
	}
	
	public void increaseRemainingSeats(Long id){
		Trip trip = tripRepository.findOne(id);
		trip.setRemainingSeats(trip.getRemainingSeats() + 1);
		tripRepository.save(trip);
	}
}
