package edu.mga.knightrider.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import edu.mga.knightrider.controller.UserRestController;
import edu.mga.knightrider.exception.UserAlreadyExistException;
import edu.mga.knightrider.exception.UserCannotDeleteException;
import edu.mga.knightrider.model.Car;
import edu.mga.knightrider.model.Role;
import edu.mga.knightrider.model.User;
import edu.mga.knightrider.model.UserRole;
import edu.mga.knightrider.repository.CarRepository;
import edu.mga.knightrider.repository.MessageRepository;
import edu.mga.knightrider.repository.TripRepository;
import edu.mga.knightrider.repository.UserRepository;

@Service
@Transactional
public class UserService {
	
	private static Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TripService tripService;
	
	@Autowired
	private CarService carService;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private PassengerService passengerService;
	
	@Autowired
	private ReviewService reviewService;		

	@Autowired
	private RoleService roleService;
	
	@Autowired
	private UserProfileService userProfileService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	public List<User> getAllUsers() {
				
		List<User> users= userRepository.findAll();
		List<User> uList = users.stream().filter(p->p.isEnabled() == true).collect(Collectors.toList());
		return uList;
	}
	
	public User getUser(String username){
		Optional<User> user = userRepository.findByUsername(username);
		return user.get();
	}
	
	public User getUser(Long id) {		
		User user = userRepository.findOne(id);
		return user;
	}

	/*
	public User addUser(User user) throws UserAlreadyExistException {
		
		logger.debug("user: " + user);
		
		if(getByUsername(user.getUsername()) == null){
		
			String encodedPassword = passwordEncoder.encode(user.getPassword());
			user.setPassword(encodedPassword);
			
			User u = userRepository.saveAndFlush(user);
			
			// add default role (member)
			Long userId = u.getId();
			UserRole role = roleService.addRole(userId, Role.MEMBER);
			List<UserRole> roles = new ArrayList<UserRole>();
			roles.add(role);
			u.setRoles(roles);

			
			return u;
		} else {
			throw new UserAlreadyExistException();
		}
	}
	*/
	
	public User updateUser(Long id, User user) {		
		user.setId(id);
		
		//User u = userRepository.getOne(id);		
		//user.setRoles(u.getRoles());
		
		// this should be enabled when users are allowed to update password
		//String encodedPassword = passwordEncoder.encode(user.getPassword());
		//user.setPassword(encodedPassword);
		
		User u = userRepository.save(user);
		
		return u;
	}
	
	
	@Transactional
	public void deleteUser(Long id) {
		
		try {
			
			logger.info("deleteInBatch(passengers) ---");
			passengerService.deletePassengersByUserId(id);
			
			logger.info("deleteInBatch(messages) ---");
			messageService.deleteByUserId(id);
			
			logger.info("deleteInBatch(reviews) ---");
			reviewService.deleteByUserId(id);
			
			logger.info("deleteInBatch(trips) ---");
			tripService.deleteTripByUserId(id);
			
			logger.info("delete user profile ---");
			userProfileService.deleteByUserId(id);	
			
			logger.info("delete role ---");
			roleService.deleteByUserId(id); 				

			logger.info("deleteInBatch(cars) ---");
			carService.deleteCarByUserId(id);
	
			userRepository.delete(id);	
			
		} catch (RuntimeException ex){
			throw new UserCannotDeleteException("Cannot delete this user.", ex.getCause());
		}
		
	}

	public Optional<User> getByUsername(String username) {
		Optional<User> user = this.userRepository.findByUsername(username);		
		return user;
    }
}
