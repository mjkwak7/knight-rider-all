package edu.mga.knightrider.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mga.knightrider.model.Car;
import edu.mga.knightrider.model.User;
import edu.mga.knightrider.repository.CarRepository;
import edu.mga.knightrider.repository.UserRepository;

@Service
public class CarService {
	
	@Autowired
	private CarRepository carRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	public List<Car> getAllCars() {	
		return carRepository.findAll();
	}
	

	public List<Car> getAllCars(Long userId) {	
		return carRepository.findByUserId(userId);
	}
	
	public Car getCar(Long id) {		
		return carRepository.findOne(id);
	}
	
	public Car addCar(Car car) {
		return carRepository.saveAndFlush(car);		
	}

	public Car addCar(Long userId, Car car) {
		
		User user = userRepository.findOne(userId);
		car.setUser(user);
		
		return carRepository.saveAndFlush(car);		
	}
	
	public Car updateCar(Long id, Car car) {		
		car.setId(id);
		return carRepository.saveAndFlush(car);
	}
	
	public Car updateCar(Long id, Long userId, Car car) {		
		car.setId(id);
		User user = userRepository.findOne(userId);
		car.setUser(user);
		return carRepository.saveAndFlush(car);
	}

	public void deleteCar(Long id) {		
		carRepository.delete(id);	
	}

	public void deleteCarByUserId(Long userId){
		List<Car> cars = getAllCars(userId);
		for (Car car : cars){
			deleteCar(car.getId());
		}
		//carRepository.deleteInBatch(carRepository.findByUserId(userId));
	}
}
