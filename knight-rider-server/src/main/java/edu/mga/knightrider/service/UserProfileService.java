package edu.mga.knightrider.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mga.knightrider.model.UserProfile;
import edu.mga.knightrider.repository.UserProfileRepository;

@Service
public class UserProfileService {
	
	@Autowired
	private UserProfileRepository userProfileRepository;
	
	public UserProfile findOne(Long id){
		
		return userProfileRepository.findOne(id);
	}
	
	public UserProfile findOneByUserId(Long id){
		
		return userProfileRepository.findByUserId(id);
	}
	
	public UserProfile insertOne(UserProfile profile){
		
		return userProfileRepository.save(profile);
	}
	
	public UserProfile updateOne(UserProfile profile){
		
		return userProfileRepository.save(profile);
	}
	
	public void deleteOne(UserProfile profile){
		
		userProfileRepository.delete(profile);
	}
	
	public void deleteOne(Long id){
		
		userProfileRepository.delete(id);
	}
	
	public void deleteByUserId(Long id){
		userProfileRepository.deleteByUserId(id);
	}
}
