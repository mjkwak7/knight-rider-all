package edu.mga.knightrider.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mga.knightrider.model.Role;
import edu.mga.knightrider.model.UserRole;
import edu.mga.knightrider.repository.RoleRepository;

@Service
public class RoleService {

	private static Logger logger = LoggerFactory.getLogger(RoleService.class);
	
	@Autowired
	private RoleRepository roleRepository;


	/*
	 * Add new role to a user
	 */
	public UserRole addRole(Long userId, Role role) {
		
		logger.debug("Add Role: " + userId + " " + role.toString());
		
		UserRole.Id id = new UserRole.Id(userId, role);
		UserRole r = new UserRole();
		r.setId(id);

		return roleRepository.saveAndFlush(r);
	}

	public List<UserRole> getAllUserRoles(Long userId){
		return roleRepository.findByUserId(userId);
	}
	
	public void deleteByUserId(Long userId){
		roleRepository.deleteByUserId(userId);
	}
	
}
