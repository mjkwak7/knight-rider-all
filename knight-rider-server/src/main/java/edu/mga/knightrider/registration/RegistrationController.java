package edu.mga.knightrider.registration;


import java.util.Locale;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.mga.knightrider.exception.InvalidOldPasswordException;
import edu.mga.knightrider.exception.UserAlreadyExistException;
import edu.mga.knightrider.mail.EmailService;
import edu.mga.knightrider.model.User;
import edu.mga.knightrider.security.dto.PasswordDto;
import edu.mga.knightrider.security.dto.UserDto;
import edu.mga.knightrider.security.model.VerificationToken;
import edu.mga.knightrider.security.service.UserSecurityService;
import edu.mga.knightrider.util.GenericResponse;

@CrossOrigin
@RestController
public class RegistrationController {

	private static Logger logger = LoggerFactory.getLogger(RegistrationController.class);
	
	@Autowired
	private UserSecurityService userSecurityService;
	
    @Autowired
    private ApplicationEventPublisher eventPublisher;
    
    @Autowired
    private Environment env;
    
    @Autowired
    public EmailService emailService;
    
    @Autowired
    private MessageSource messages;      
	
	@RequestMapping(method=RequestMethod.POST, value="/knightrider/auth/register", consumes= MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GenericResponse register(@Valid @RequestBody UserDto userDto) {		
		if(logger.isDebugEnabled()) {
            logger.debug("user registration");
        }			
		
		if(userSecurityService.findUserByEmail(userDto.getEmail()).isPresent()){
			
			if(logger.isDebugEnabled()) {
	            logger.debug("Email already exists");
	        }
			
			//new GenericResponse(userDto.getEmail(), "Email already exists. Please use anoother valid email account.");
			throw new UserAlreadyExistException("Email adress (" + userDto.getEmail() + ") already exists!");
		}

		final User registered = userSecurityService.registerNewUserAccount(userDto);		
		
		String basePath = env.getProperty("server.host") + ":" + env.getProperty("server.port");
		Locale loc = new Locale("en", "US");
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, loc, basePath));        
		
		return new GenericResponse("Success. Please check your email.");
	}	
	

    // user activation - verification

    @RequestMapping(value = "/knightrider/auth/resendRegistrationToken", method = RequestMethod.GET)
    @ResponseBody
    public GenericResponse resendRegistrationToken(@RequestParam("token") final String existingToken) {
        final VerificationToken newToken = userSecurityService.generateNewVerificationToken(existingToken);
        final User user = userSecurityService.getUser(newToken.getToken());
        Locale loc = new Locale("en", "US");
        
        //emailService..send(constructResendVerificationTokenEmail(basePath, loc, newToken, user));
        return new GenericResponse(messages.getMessage("message.resendToken", null, loc));
    }

    // change user password
    @RequestMapping(value = "/knightrider/auth/updatePassword", method = RequestMethod.POST)
    @ResponseBody
    public GenericResponse changeUserPassword(final Locale locale, @Valid PasswordDto passwordDto) {
        final Optional<User> user = userSecurityService.findUserByEmail(((User) SecurityContextHolder.getContext()
            .getAuthentication()
            .getPrincipal()).getUsername());
        if (!userSecurityService.checkIfValidOldPassword(user.get(), passwordDto.getOldPassword())) {
            throw new InvalidOldPasswordException();
        }
        userSecurityService.changeUserPassword(user.get(), passwordDto.getNewPassword());
        return new GenericResponse(messages.getMessage("message.updatePasswordSuc", null, locale));
    }



    // ============== NON-API ============

    /*
    private SimpleMailMessage constructResendVerificationTokenEmail(final String contextPath, final Locale locale, final VerificationToken newToken, final User user) {
        final String confirmationUrl = contextPath + "/registrationConfirm.html?token=" + newToken.getToken();
        final String message = messages.getMessage("message.resendToken", null, locale);
        return constructEmail("Resend Registration Token", message + " \r\n" + confirmationUrl, user);
    }

    private SimpleMailMessage constructResetTokenEmail(final String contextPath, final Locale locale, final String token, final User user) {
        final String url = contextPath + "/user/changePassword?id=" + user.getId() + "&token=" + token;
        final String message = messages.getMessage("message.resetPassword", null, locale);
        return constructEmail("Reset Password", message + " \r\n" + url, user);
    }
    

    private SimpleMailMessage constructEmail(String subject, String body, User user) {
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject(subject);
        email.setText(body);
        email.setTo(user.getUsername());
        email.setFrom(env.getProperty("support.email"));
        return email;
    }

*/
}
