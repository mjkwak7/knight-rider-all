package edu.mga.knightrider.registration;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import edu.mga.knightrider.model.User;
import edu.mga.knightrider.security.service.UserSecurityService;
import edu.mga.knightrider.service.UserService;

@CrossOrigin
@Controller
public class RegistrationConfirmController {
	
	private static Logger logger = LoggerFactory.getLogger(RegistrationConfirmController.class);
	
	@Autowired
    private Environment env;
	
	@Autowired
	private UserSecurityService userService;
	
	@RequestMapping(value = "/knightrider/auth/registrationConfirm", method = RequestMethod.GET)
	public ModelAndView confirmRegistration(ModelAndView model, @RequestParam("token") final String token) throws UnsupportedEncodingException {

		
		final String result = userService.validateVerificationToken(token);
        if (result.equals("valid")) {
            final User user = userService.getUser(token);
            
            logger.debug(user.toString());
            model.addObject("message", "Successfully validated!");
            
        } else {
        	model.addObject("message", "Validation failed!");
        }
        
        String webappUrl = env.getProperty("webapp.url");
        
        //httpServletResponse.setHeader("Location", webappUrl);
        model.addObject("url", webappUrl);
        model.setViewName("knightrider/registrationconfirmresult");
        
        return model;
    }

}
