package edu.mga.knightrider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KnightRiderApplication {

	public static void main(String[] args) {
		SpringApplication.run(KnightRiderApplication.class, args);
	}
	
}
