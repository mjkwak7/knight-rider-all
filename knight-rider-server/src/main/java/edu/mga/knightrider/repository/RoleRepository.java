package edu.mga.knightrider.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.mga.knightrider.model.UserRole;

public interface RoleRepository extends JpaRepository<UserRole, Long> {

	@Query("select u from UserRole u where u.userId = :userId")
    public List<UserRole> findByUserId(@Param("userId") Long userId); 
	
	@Modifying
	@Query("delete from UserRole u where u.userId = :userId")
    public void deleteByUserId(@Param("userId") Long userId);
	
}
