package edu.mga.knightrider.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.mga.knightrider.model.Message;

public interface MessageRepository extends JpaRepository<Message, Long> {

	@Query("select p from Message p where p.tripId=:tripId and p.userId=:userId")
    public List<Message> findByTripIdAndUserId(@Param("tripId") Long tripId, @Param("userId") Long userId);
	
	@Query("select p from Message p where p.tripId=:tripId")
    public List<Message> findByTripId(@Param("tripId") Long tripId);
	
	@Query("select p from Message p where p.userId=:userId")
    public List<Message> findByUserId(@Param("userId") Long userId);
	
	@Modifying
	@Query("delete from Message p where p.tripId=:tripId")
    public void deleteByTripId(@Param("tripId") Long tripId);
	
	@Modifying
	@Query("delete from Message p where p.userId=:userId")
    public void deleteByUserId(@Param("userId") Long userId);
	
	@Modifying
	@Query("delete from Message p where p.tripId=:tripId and p.userId=:userId")
    public void deleteByTripIdAndUserId(@Param("tripId") Long tripId, @Param("userId") Long userId);
}
