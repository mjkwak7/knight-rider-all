package edu.mga.knightrider.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.mga.knightrider.model.Trip;

public interface TripRepository extends JpaRepository<Trip, Long>, JpaSpecificationExecutor<Trip> {
	
	List<Trip> findByUserId(Long userId);

	@Query("select t from Trip t where t.car.id = :carId")
    public List<Trip> findByCarId(@Param("carId") Long carId);
	
}
