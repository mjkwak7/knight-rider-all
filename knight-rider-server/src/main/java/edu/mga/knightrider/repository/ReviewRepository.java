package edu.mga.knightrider.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.mga.knightrider.model.Review;

public interface ReviewRepository extends JpaRepository<Review, Long> {

	@Query("select r from Review r where r.tripId=:tripId and r.userId=:userId")
    public List<Review> findByReviewsIdAndUserId(@Param("tripId") Long tripId, @Param("userId") Long userId);
	
	@Query("select r from Review r where r.tripId=:tripId")
    public List<Review> findReviewsByTripId(@Param("tripId") Long tripId);
	
	@Query("select r from Review r where r.userId=:userId")
    public List<Review> findByReviewsUserId(@Param("userId") Long userId);
	
	@Modifying
	@Query("delete from Review r where r.tripId=:tripId")
    public void deleteByTripId(@Param("tripId") Long tripId);
	
	@Modifying
	@Query("delete from Review r where r.userId=:userId")
    public void deleteByUserId(@Param("userId") Long userId);
	
	@Modifying
	@Query("delete from Review r where r.tripId=:tripId and r.userId=:userId")
    public void deleteByTripIdAndUserId(@Param("tripId") Long tripId, @Param("userId") Long userId);
	
	
}
