package edu.mga.knightrider.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.mga.knightrider.model.Car;

public interface CarRepository extends JpaRepository<Car, Long> {
	 
	List<Car> findByUserId(Long id);
	 
}
