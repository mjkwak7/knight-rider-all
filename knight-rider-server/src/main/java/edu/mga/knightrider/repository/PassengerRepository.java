package edu.mga.knightrider.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;



import edu.mga.knightrider.model.Passenger;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {

	//Optional<User> findByLastName(String lastName);
	
	//@Query("select u from User u left join fetch u.roles r where u.username=:username")
    //public Optional<User> findByUsername(@Param("username") String username);
	
	//Passenger findByUserId(Long id);
	//Passenger findByTripId(Long id);
	
	
	@Query("select p from Passenger p where p.tripId=:tripId and p.userId=:userId")
    public Passenger findByPrimaryKey(@Param("tripId") Long tripId, @Param("userId") Long userId);
	
	@Query("select p from Passenger p where p.userId=:userId")
    public List<Passenger> findByUserId(@Param("userId") Long userId);
	
	@Modifying
	@Query("delete from Passenger p where p.tripId=:tripId and p.userId=:userId")
    public void deleteByPrimaryKey(@Param("tripId") Long tripId, @Param("userId") Long userId);
	
}
