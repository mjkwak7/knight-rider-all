package edu.mga.knightrider.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.mga.knightrider.model.UserProfile;

public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {
	
	UserProfile findByUserId(Long id);
	
	@Modifying
	@Query("delete from UserProfile u where u.user.id = :userId")
    public void deleteByUserId(@Param("userId") Long userId);
	
}
