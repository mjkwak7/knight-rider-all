package edu.mga.knightrider.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import edu.mga.knightrider.exception.ProfilePictureUploadException;

@Service
public class ImageUtil {

	private static Logger logger = LoggerFactory.getLogger(ImageUtil.class);
	
	@Autowired
    private Environment env;
	
	public String saveBase64ImageString(String base64ImageString, Long userId, String extension){
		
		String webapphost = env.getProperty("webapp.url");
		String serverDirectory = env.getProperty("server.directory");
		String imgDirectory = env.getProperty("image.directory");
		
		//String imagePath = "C:\\base64\\image.jpg";
		//System.out.println("=================Encoder Image to Base 64!=================");
		//String base64ImageString = encoder(imagePath);
		//System.out.println("Base64ImageString = " + base64ImageString);
 
		//System.out.println("=================Decoder Base64ImageString to Image!=================");
		//decoder(base64ImageString, "C:\\base64\\decoderimage.jpg");
 
		//System.out.println("DONE!");
		
		if (extension == null || extension.isEmpty()){
			extension = "jpg";
		}
		String fileName = "profile_picture_" + userId + "." + extension;
		String imgFilePath = serverDirectory + imgDirectory + "/" + fileName;
		
		logger.info("Image File Path --- " + imgFilePath);
		
		File file = new File(imgFilePath);
		if (file.exists()){
			file.delete();
		}
		
		decoder(base64ImageString, imgFilePath);
		
		String webUrl = webapphost + imgDirectory + "/" + fileName;
		
		logger.info("Web URL --- " + webUrl);
		
		return webUrl;
	}
	
	public String encoder(String imagePath) {
		String base64Image = "";
		File file = new File(imagePath);
		try (FileInputStream imageInFile = new FileInputStream(file)) {
			// Reading a Image file from file system
			byte imageData[] = new byte[(int) file.length()];
			imageInFile.read(imageData);
			base64Image = Base64.getEncoder().encodeToString(imageData);
		} catch (FileNotFoundException e) {
			System.out.println("Image not found" + e);
		} catch (IOException ioe) {
			System.out.println("Exception while reading the Image " + ioe);
		}
		return base64Image;
	}

	public void decoder(String base64Image, String pathFile) {
		
		logger.info("Decoding --- " + pathFile);
		
		try (FileOutputStream imageOutFile = new FileOutputStream(pathFile)) {
			// Converting a Base64 String into Image byte array
			byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
			imageOutFile.write(imageByteArray);
		} catch (FileNotFoundException e) {
			logger.info("FileNotFoundException " + e.getMessage());
			//System.out.println("Image not found" + e);
			throw new ProfilePictureUploadException("Image not found");
		} catch (IOException ioe) {
			//System.out.println("Exception while reading the Image " + ioe);
			logger.info("IOException " + ioe.getMessage());
			throw new ProfilePictureUploadException("Cannot read the Image");
		}
	}

}
