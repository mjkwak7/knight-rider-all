package edu.mga.knightrider.exception;

public final class TripJoinException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3805525777024323686L;

	 public TripJoinException() {
	        super();
	    }

	    public TripJoinException(final String message, final Throwable cause) {
	        super(message, cause);
	    }

	    public TripJoinException(final String message) {
	        super(message);
	    }

	    public TripJoinException(final Throwable cause) {
	        super(cause);
	    }
}
