package edu.mga.knightrider.exception;

public final class UserMismatchException extends RuntimeException {

    private static final long serialVersionUID = 5861310537366287163L;

    public UserMismatchException() {
        super();
    }

    public UserMismatchException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UserMismatchException(final String message) {
        super(message);
    }

    public UserMismatchException(final Throwable cause) {
        super(cause);
    }    
}
