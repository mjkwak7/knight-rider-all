package edu.mga.knightrider.exception;

public final class CarUpdateException extends RuntimeException {

	private static final long serialVersionUID = 3805525777024323686L;

	public CarUpdateException() {
		super();
	}

	public CarUpdateException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public CarUpdateException(final String message) {
		super(message);
	}

	public CarUpdateException(final Throwable cause) {
		super(cause);
	}
}
