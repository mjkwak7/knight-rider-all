package edu.mga.knightrider.exception;

public class TripAddNoCarInfoException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3805525777024323686L;

	 public TripAddNoCarInfoException() {
	        super();
	    }

	    public TripAddNoCarInfoException(final String message, final Throwable cause) {
	        super(message, cause);
	    }

	    public TripAddNoCarInfoException(final String message) {
	        super(message);
	    }

	    public TripAddNoCarInfoException(final Throwable cause) {
	        super(cause);
	    }

}
