package edu.mga.knightrider.exception;

public final class TripDeleteException extends RuntimeException {

	private static final long serialVersionUID = 3805525777024323686L;

	 public TripDeleteException() {
	        super();
	    }

	    public TripDeleteException(final String message, final Throwable cause) {
	        super(message, cause);
	    }

	    public TripDeleteException(final String message) {
	        super(message);
	    }

	    public TripDeleteException(final Throwable cause) {
	        super(cause);
	    }
}
