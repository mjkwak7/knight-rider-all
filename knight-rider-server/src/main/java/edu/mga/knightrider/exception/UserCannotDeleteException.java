package edu.mga.knightrider.exception;

public final class UserCannotDeleteException extends RuntimeException {

	private static final long serialVersionUID = 5861310537366287163L;

    public UserCannotDeleteException() {
        super();
    }

    public UserCannotDeleteException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UserCannotDeleteException(final String message) {
        super(message);
    }

    public UserCannotDeleteException(final Throwable cause) {
        super(cause);
    }
    
}
