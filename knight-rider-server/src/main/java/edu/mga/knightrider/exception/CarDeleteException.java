package edu.mga.knightrider.exception;

public final class CarDeleteException extends RuntimeException {

	private static final long serialVersionUID = 3805525777024323686L;

	public CarDeleteException() {
		super();
	}

	public CarDeleteException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public CarDeleteException(final String message) {
		super(message);
	}

	public CarDeleteException(final Throwable cause) {
		super(cause);
	}
}
