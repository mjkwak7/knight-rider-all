package edu.mga.knightrider.exception;

public final class TripUpdateException extends RuntimeException {

	private static final long serialVersionUID = 3805525777024323686L;

	 public TripUpdateException() {
	        super();
	    }

	    public TripUpdateException(final String message, final Throwable cause) {
	        super(message, cause);
	    }

	    public TripUpdateException(final String message) {
	        super(message);
	    }

	    public TripUpdateException(final Throwable cause) {
	        super(cause);
	    }
}
