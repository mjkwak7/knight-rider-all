package edu.mga.knightrider.exception;

public final class TripNotFoundException extends RuntimeException {   

    /**
	 * 
	 */
	private static final long serialVersionUID = 8654383766573070698L;

	public TripNotFoundException() {
        super();
    }

    public TripNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public TripNotFoundException(final String message) {
        super(message);
    }

    public TripNotFoundException(final Throwable cause) {
        super(cause);
    }

}
