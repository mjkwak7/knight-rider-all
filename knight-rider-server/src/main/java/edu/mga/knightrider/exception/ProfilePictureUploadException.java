package edu.mga.knightrider.exception;

public class ProfilePictureUploadException extends RuntimeException {

	private static final long serialVersionUID = 5861310537366287163L;

    public ProfilePictureUploadException() {
        super();
    }

    public ProfilePictureUploadException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ProfilePictureUploadException(final String message) {
        super(message);
    }

    public ProfilePictureUploadException(final Throwable cause) {
        super(cause);
    }
}
